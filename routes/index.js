// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });
// /* GET about page. */
// router.get('/about', function(req, res) {
//   res.render('pages/about');
// });
// module.exports = router;
module.exports = function(app) {
  
  app.get('/', function(req, res) {

    if(req.query.er){
  if(req.query.er!=null ||req.query.er!=undefined){

    res.cookie('er',req.query.er);
  
  }
}
res.render('LandingPage');
});
  app.get('/PrivacyPolicy', function(req, res) {
    res.render('partials/PrivacyPolicy');
  });
  app.get('/Contactus', function(req, res) {
    res.render('pages/Contactus');
  });
  app.get('/Aboutus', function(req, res) {
    res.render('pages/Aboutus');
  });
  app.get('/TermsCondition', function(req, res) {
    res.render('partials/TermsCondition');
  });
  app.get('/Disclaimer', function(req, res) {
    res.render('partials/Disclaimer');
  });
  app.get('/FAQs', function(req, res) {
    res.render('partials/FAQs');
  });
  app.get('/Registration', function(req, res) {
    res.render('pages/EmployerZone/EmployerRegistration');
  });

  app.get('/login', function(req, res) {
    req.session.login = "Loggedout";
    res.render('pages/EmployerZone/Employerlogin');
  });
  app.get('/Employerforgot', function(req, res) {
    res.render('pages/EmployerZone/Employerforgot');
  });
  app.get('/ChangePassword', function(req, res) {
    res.render('pages/EmployerZone/ChangePassword');
  });
  app.get('/Empprofile', function(req, res) {
    res.render('pages/EmployerZone/Empprofile');
  });
  app.get('/ResetPassword', function(req, res) {
    res.render('pages/EmployerZone/ResetPassword');
  });
  app.get('/vacancyjob', function(req, res) {
    res.render('pages/vacancy/vacancyjob');
  });
  app.get('/Edit', function(req, res) {
    res.render('pages/vacancy/Edit');
  });
  app.get('/PostJob', function(req, res) {
    res.render('pages/Job/PostJob');
  });
  app.get('/Jobpost', function(req, res) {
    res.render('pages/Job/PostJob_one');
  });
  app.get('/buyPackage', function(req, res) {
    res.render('pages/Onlinebuy/packages');
  });
  app.get('/buyPackageRegistration', function(req, res) {
    res.render('pages/Onlinebuy/Registration');
  });
  app.get('/buyPackageLogin', function(req, res) {
    res.render('pages/Onlinebuy/Login');
  });
  app.get('/Dashboard', function(req, res) {
    req.session.login = "Loggedin";
    res.render('pages/Home');
  });
  app.get('/Jopposting', function(req, res) {
    res.render('pages/Job/Jopposting');
  });
  app.get('/Employerlogin', function(req, res) {
    res.render('pages/EmployerZone/login');
  });
  app.get('/ResumeDatabaseAccess', function(req, res) {
    res.render('pages/resdex/ResumeDatabaseAccess');
  });
  app.get('/SuccessRegistration', function(req, res) {
    res.render('pages/SuccessRegistration');
  });
  app.get('/Profile', function(req, res) {
    res.render('pages/Profile');
  });
  app.get('/desired', function(req, res) {
    res.render('pages/desiredskill');
  });
  app.get('/SearchCandidate', function(req, res) {
    res.render('pages/SearchCandidate',
    // {
    // Experience: req.cookies.Experience,
    // DesignationId: req.cookies.DesignationId,
    // Qualifid: req.cookies.Qualif_id,
    // SubGroupid: req.cookies.Sub_Group_id,
    // DistrictId: req.cookies.DistrictId,
    // SkillId: req.cookies.SkillId 
    // }
    );
    
    
   

  });
  app.get('/Companydetail', function(req, res) {
    res.render('pages/Companydetail');
  });
  app.get('/candidatelisting', function(req, res) {
    res.render('pages/candidatelisting');   
  });
  app.get('/CVTemplate', function(req, res) {
    res.render('pages/CVTemplate');   
  });
  
  app.get('/succesreg', function(req, res) {
    res.render('pages/succesreg');   
  });

  app.get('/CandidateslistbySearch', function(req, res) {
    res.render('pages/CandidateslistbySearch');   
  });
  app.get('/EmployerLogo', function(req, res) {
    res.render('pages/EmployerLogo');   
  });
  app.get('/EditEmpProfile', function(req, res) {
    res.render('pages/EmployerZone/EditEmpProfile');   
  });
  app.get('/GSTUpload', function(req, res) {
    res.render('pages/EmployerZone/GSTUpload');   
  });

  app.get('/MobileModal', function(req, res) {
    res.render('pages/EmployerZone/MobileModal');
  });

  app.get('/emailmodal', function(req, res) {
    res.render('pages/EmployerZone/emailmodal');
  });
  app.get('/ER1Form', function(req, res) {
    res.render('pages/ER1Form');
  });
  app.get('/SearchCandidateReports', function(req, res) {
    res.render('pages/SearchCandidateReports');
  }); 
  app.get('/ER1Report', function(req, res) {
    res.render('pages/ER1Report');
  });
  app.get('/ER1ReportDetails', function(req, res) {
    res.render('pages/ER1ReportDetails');
  });

  app.get('/CandidateDetails', function(req, res) {
    res.render('pages/CandidateDetails');
  });

  app.get('/dashboardnew', function(req, res) {
    res.render('pages/dashboardnew');
  });

  app.get('/candidatelist', function(req, res) {
    res.render('pages/candidatelist');
  });

  app.get('/ManageUser', function(req, res) {
    res.render('pages/ManageUser');
  });

  app.get('/accountDetails', function(req, res) {
    res.render('pages/accountDetails');
  });

  app.get('/Jobfairdetails', function(req, res) {
    res.render('pages/Jobfair/Jobfairdetails');
  });
  app.get('/SearchParticipant', function(req, res) {
    res.render('pages/Jobfair/SearchParticipant');
  });
  app.get('/JobfairParticipantReport', function(req, res) {
    res.render('pages/Jobfair/JobfairParticipantReport');
  });
  app.get('/EmpFolders', function(req, res) {
    res.render('pages/EmpFolders');
  });

}



