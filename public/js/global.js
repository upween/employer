var globalString = "This can be accessed anywhere!";
//var serverpath = "https://rojgarportal.herokuapp.com/";
var JobseekerPath=   "http://mprojgar.gov.in/";
//var globalpath = "https://rojgar-employer.herokuapp.com/";
//var JobseekerPath = "http://stellartechedu.com/";
//var globalpath = "http://employer.stellartechedu.com/";
//var globalpath = "http://182.70.254.93:400/";
//var serverpath = "http://fit4umall.in/api/";
var serverpath = "http://localhost:3003/";
//var serverpath="http://182.70.254.93:303/"
//var serverpath = "http://mprojgar.gov.in/api/";
var globalpath = "http://localhost:4000/";
//fit4umall.in/api
//employerurl.fit4umall.in
var isValidate=false;
var isCaptchaValidated=false;
var globalemailid = ''
var globalemailpass = ''


function securedajaxget(path, type, comment, control) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata), control);
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                window[type](JSON.stringify(errordata.responseJSON), control);
                return true;
            }
            else {
                window[type](JSON.stringify(errordata), control);
                return true;
            }
        }
    });
}
function securedajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        headers: { 'authorization': sessionStorage.getItem("token"), 'refreshToken': sessionStorage.getItem("refreshToken") },
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else if (errordata.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxget(path, type, comment) {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: path,
        cache: false,
        dataType: "json",
        success: function (successdata) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function ajaxpost(path, type, comment, masterdata, control) {
    jQuery.ajax({
        url: path,
        type: "POST",
        data: masterdata,
        contentType: "application/json; charset=utf-8",
        success: function (successdata, status, jqXHR) {
            window[type](JSON.stringify(successdata));
        },
        error: function (errordata) {
            if (errordata.status == 0) {
                toastr.warning("ERR_CONNECTION_REFUSED", "", "info")
                return true;
            }
            else {
                window[type](JSON.stringify(errordata));
            }
        }
    });
}
function getvalidated(controlid, type, format) {
    var validid = "valid" + controlid;
    if (type == "text") {
        if ($("#" + controlid).val() == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "select") {
        if ($("#" + controlid).val() == "0") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else if ($("#" + controlid).val() == null) {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Select " + format);
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("");
        }
    }
    else if (type == "email") {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
        if ($("#" + controlid).val() == "") {
            $("#" + controlid).css('border-color', 'red');
            $("#" + validid).html("Please Enter " + format);
        }
        else if ($("#" + controlid).val() != "") {
            if ($("#" + controlid).val().match(mailformat)) {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
                return true;
            }
            else {
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter correct " + format);
                return false;
            }
        }
        else {
            jQuery("#" + controlid).css('border-color', '');
            $("#" + validid).html("")
            return true;
        }
    }
    else if (type == "number") {
        if (format == "Mobile Number") {
            if ($("#" + controlid).val() == "") {
                $("#" + controlid).css('border-color', 'red');
                $("#" + validid).html("Please Enter " + format);
            }
            else if ($("#" + controlid).val() != "") {
                var phoneno = /^\d{10}$/;
                if ($("#" + controlid).val().match(phoneno)) {
                    jQuery("#" + controlid).css('border-color', '');
                    $("#" + validid).html("");
                    return true;
                }
                else {
                    $("#" + controlid).css('border-color', 'red');
                    $("#" + validid).html("Please Enter correct " + format);
                    return false;
                }
            }
            else {
                jQuery("#" + controlid).css('border-color', '');
                $("#" + validid).html("");
            }
        }
    }

}
function onlyNumbers(event) {
    if (event.type == "paste") {
        var clipboardData = event.clipboardData || window.clipboardData;
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData)) {
            event.preventDefault();

        } else {
            return;
        }
    }
    var keyCode = event.keyCode || event.which;
    if (keyCode == 9) {
        return true;
    }
    if ((keyCode >= 96 && keyCode <= 105)) {
        keyCode -= 48;
    }
    if ((keyCode >= 48 && keyCode <= 57)) {
        return true;
    }
    var charValue = String.fromCharCode(keyCode);
    if (isNaN(parseInt(charValue)) && event.keyCode != 8) {
        event.preventDefault();
    }
}
function myFunction(my) {
    if (my.text == "English") {
        $(my).text("Hindi");
        window.lang.change('en');
        return false;
    }
    else {
        $(my).text("English");
        window.lang.change('th');
        return false;
    }
}
function onlyAlphabets(event) {
    //allows only alphabets in a textbox
    if (event.type == "paste") {
        var clipboardData = event.clipboardData || window.clipboardData;
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData)) {
            return;

        } else {
            event.prevetDefault();
        }
    }
    var charCode = event.which;
    if (!(charCode >= 65 && charCode <= 120) && (charCode != 32 && charCode != 0) && charCode != 8 && (event.keyCode >= 96 && event.keyCode <= 105)) {
        event.preventDefault();
    }
}


function checkLength(Id, formatlength, length) {
    var textbox = document.getElementById(Id);
    if (textbox.value.length >= length) {
        $("#" + Id).css('border-color', '');
        $("#valid" + Id).html("")
        return true;
    }
    else {
        $("#" + Id).css('border-color', 'red');
        if (formatlength == 'Pin') {
            $("#valid" + Id).html(formatlength + " Must Be Contain 6 Digits")
        }
        else {

            $("#valid" + Id).html(formatlength + " Must Contain Atleast " + length + " Characters")
        }


        return false;
    }
}
function EmailConfig() {
    var path = serverpath + "emailconfig"
    securedajaxget(path, 'parsedatasecuredEmailConfig', 'comment', 'control');
}

function parsedatasecuredEmailConfig(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        EmailConfig();
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        globalemailid = data[0][0].Email
        globalemailpass = data[0][0].Password
    }

}
document.getElementsByClassName('yatmlogo').src = JobseekerPath + 'images/YATMLogo.png';
document.getElementsByClassName('mplogo').src = JobseekerPath + 'images/logo1.png';
function jobseekerurl() {
    window.location = JobseekerPath;
}