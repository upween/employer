$.fn["clientSidePagination"] = function(options) {
  options = options || {};

  var self = this;

  $(this).data("page", 1);
  $(this).data("per_page", $(this).attr("per-page") || 15);
  $(this).data("num", $(".i", $(self)).length);
  if( $(".i", $(self)).length){
    if ($(this)[0].localName == "tbody"){
      var collength = $(this)[0].firstElementChild.cells.length;
      $(this).append("<tr class='center1'><td colspan="+collength+" class='pagination'><a href='#' class='previous'>&laquo;</a><a href='#' class='number active i1'>1</a><a href='#' class='number i2'>2</a><a href='#' class='number i3'>3</a><a href='#' class='number i4'>4</a><a href='#' class='number i5'>5</a><a href='#' class='next'>&raquo;</a><span class='current'></span> </td></tr>");
    }
    else{
      $(this).append("<tr class='center1'><td class='pagination'><a href='#' class='previous'>&laquo;</a><a href='#' class='number active i1'>1</a><a href='#' class='number i2'>2</a><a href='#' class='number i3'>3</a><a href='#' class='number i4'>4</a><a href='#' class='number i5'>5</a><a href='#' class='next'>&raquo;</a><span class='current'></span> </td></tr>");
    }
    showPage(1);
  }
  


  $(".next", $(this)).bind("click", nextPage);
  $(".previous", $(this)).bind("click", previousPage);
  $(".number", $(this)).bind("click", numberPage);

  function numberPage(event){
    event.preventDefault();
    var numclick = event.currentTarget.text;
    current = numclick;
    if (Math.ceil($(self).data("num") / $(self).data("per_page")) >= current)
      showPage(current);
  }

  function nextPage(event) {
      event.preventDefault();
      current = $(self).data("page");
      if (Math.ceil($(self).data("num") / $(self).data("per_page")) > current)
          showPage(Math.ceil(current) + 1);
  }

  function previousPage(event) {
      event.preventDefault();
      current = $(self).data("page");
      if (current > 1){
          showPage(Math.ceil(current) - 1);
    }
  }

  function showPage(page) {
      $(self).data("page", page);
      $(".i", $(self)).hide();
      for(i=0; i<$(self).data("per_page"); i++) {
         

       
          index = $(self).data("per_page") * ($(self).data("page")-1) + i;
          if (index < $(self).data("num"))
              $($(".i", $(self))[index]).show();
          $(".pagination .current", $(self)).html(" Total no of pages :- "+Math.ceil($(self).data("num") / $(self).data("per_page")));
       
         }
         $(".number").removeClass("active");
    
    
        
    
      var divhide = Math.ceil($(self).data("num") / $(self).data("per_page"));  
      var lasttot = divhide-4;
if (divhide == 1){
  $(".i"+page).addClass("active"); 
  $(".i2").hide();
  $(".i3").hide();
  $(".i4").hide();
  $(".i5").hide();
}
else if (divhide == 2){
  $(".i"+page).addClass("active"); 
  $(".i3").hide();
  $(".i4").hide();
  $(".i5").hide();
}
else if (divhide == 3){
  $(".i"+page).addClass("active"); 
  $(".i4").hide();
  $(".i5").hide();
}
else if (divhide == 4){
  $(".i"+page).addClass("active"); 
  $(".i5").hide();
}
else{
  $(".i1").show();
  $(".i2").show();
  $(".i3").show();
  $(".i4").show();
  $(".i5").show();
if (page >= lasttot ){
  $(".i1").html(Math.ceil(lasttot));
  $(".i2").html(Math.ceil(lasttot)+1);
  $(".i3").html(Math.ceil(lasttot)+2);
  $(".i4").html(Math.ceil(lasttot)+3);
  $(".i5").html(Math.ceil(lasttot)+4);
  for(i=0; i<5; i++) {
if (page == lasttot+i){
  var j=1;
  j=j+i;
  $(".i"+j).addClass("active");
}
  }
  // 
}
else{
  $(".i1").html(Math.ceil(page));
  $(".i2").html(Math.ceil(page)+1);
  $(".i3").html(Math.ceil(page)+2);
  $(".i4").html(Math.ceil(page)+3);
  $(".i5").html(Math.ceil(page)+4);
  $(".i1").addClass("active"); 
}
  
  
}
      
  }

};