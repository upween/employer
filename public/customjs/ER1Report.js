function FillYear(funct,control) {
    var path =  serverpath + "secured/year/0/10"
    securedajaxget(path,funct,'comment',control);
  }
  function parsedatasecuredFillYear(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillYear('parsedatasecuredFillYear','yearddl');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
       else if(data.errno) {
            toastr.warning("Something went wrong Please try again later", "", "info")
            return false;
        }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Year"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FinYearId).html(data1[i].Year));
             }
        }
              
  }

  function FillMonthER1(funct,control) {
    var path =  serverpath + "month/0"
    securedajaxget(path,funct,'comment',control);
  }
  
  function parsedatasecuredFillMonthER1(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillMonthER1('parsedatasecuredFillMonthER1',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
  
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Month"));
            for (var i = 0; i < data1.length; i++) {
              if(data1[i].FullMonthName=='March' ||data1[i].FullMonthName=='June' ||data1[i].FullMonthName=='September' ||data1[i].FullMonthName=='December' ){
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FullMonthName).html(data1[i].FullMonthName));
           
              }
             }
  
            
        }
              
  }
  function FetchER1Form() {

   
      
        var path =  serverpath + "geter1formbyEmp/"+$('#yearddl').val()+"/"+$('#monthddl').val()+"/"+sessionStorage.NewEmployerId+""
       ajaxget(path,'parsedatasFetchER1Form','comment','control');
    }
    
    function parsedatasFetchER1Form(data,control){  
        data = JSON.parse(data)
        if (data.message == "New token generated"){
            sessionStorage.setItem("token", data.data.token);
            FetchER1Form();
        }
        else if (data.status == 401){
            toastr.warning("Unauthorized", "", "info")
            return true;
        }
            else{
                jQuery("#"+control).empty();
                var data1 = data[0];
                var appenddata='';
                for (var i = 0; i < data1.length; i++) {
            appenddata+=`<tr class='i'>
                                <td>`+[i+1]+`</td>
                                <td>`+data1[i].Name+`</td>
                                <td>`+data1[i].Address+`</td>
                                <td>`+data1[i].OfficeType+`</td>
                                <td>`+data1[i].Date+`</td>
                                <td>
                                <button type="button" onclick=viewsreport(`+data1[i].EmpId+`,'${encodeURI(data1[i].Name)}','${encodeURI(data1[i].Address)}','${encodeURI(data1[i].OfficeType)}','${encodeURI(data1[i].PreQuaterMen)}','${encodeURI(data1[i].PreQuaterWomen)}','${encodeURI(data1[i].PreQuaterTotal)}','${encodeURI(data1[i].UndQuaterWomen)}','${encodeURI(data1[i].UndQuaterTotal)}','${encodeURI(data1[i].IncDecQuater)}','${encodeURI(data1[i].Reason)}','${encodeURI(data1[i].Signature)}','${encodeURI(data1[i].Place)}','${encodeURI(data1[i].BuisnessNature)}','${encodeURI(data1[i].HR_name)}','${encodeURI(data1[i].Contactnumber)}','${encodeURI(data1[i].EmailId)}','${encodeURI(data1[i].Month)}','${encodeURI(data1[i].Year)}')  class="btn btn-success" style="background-color:#716aca;border-color:#716aca;"> View/Edit Form</button>
                                </td>
                        </tr>`;
                }
                $('#tbodyvalue').append(appenddata);
                $('#tbodyvalue').clientSidePagination();
            }
                 
    }
    function viewsreport(empid,Name,Address,OfficeType,PreQuaterMen,PreQuaterWomen,PreQuaterTotal,UndQuaterWomen,UndQuaterTotal,IncDecQuater,Reason,Signature,Place,BuisnessNature,HR_name,Contactnumber,EmailId,Month,Year){
        sessionStorage.setItem('empid',empid)
        sessionStorage.er1Name=decodeURI(Name),
        sessionStorage.er1Address=decodeURI(Address),
        sessionStorage.er1OfficeType=decodeURI(OfficeType),
        sessionStorage.er1PreQuaterMen=decodeURI(PreQuaterMen),
        sessionStorage.er1PreQuaterWomen=decodeURI(PreQuaterWomen),
        sessionStorage.er1PreQuaterTotal=decodeURI(PreQuaterTotal),
        sessionStorage.er1UndQuaterWomen=decodeURI(UndQuaterWomen),
        sessionStorage.er1UndQuaterTotal=decodeURI(UndQuaterTotal),
        sessionStorage.er1IncDecQuater=decodeURI(IncDecQuater),
        sessionStorage.er1Reason=decodeURI(Reason),
        sessionStorage.er1Signature=decodeURI(Signature),
        sessionStorage.er1Place=decodeURI(Place),
        sessionStorage.er1BuisnessNature=decodeURI(BuisnessNature),
        sessionStorage.er1HR_name=decodeURI(HR_name),
        sessionStorage.er1Contactnumber=decodeURI(Contactnumber),
        sessionStorage.er1EmailId=decodeURI(EmailId),
        sessionStorage.er1Month=decodeURI(Month),
        sessionStorage.er1Year=decodeURI(Year),
        sessionStorage.Type='Edit',
        window.location='/ER1Form';
        
    }