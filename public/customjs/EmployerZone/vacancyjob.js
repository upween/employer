var code;
function createCaptchaempreg() {

    //clear the contents of captcha div first 
    document.getElementById('captchaEmployerRegistration').innerHTML = "";
    var charsArray =
        "0123456789";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
        //below code will not allow Repetition of Characters
        var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
        if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
        else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captchaEmployerRegistration";
    canv.width = 100;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    code = captcha.join("");
    document.getElementById("captchaEmployerRegistration").appendChild(canv); // adds the canvas to the body element
}
function FillSkillSet(funct,control) {
    var path =  serverpath + "SkillSet/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSkillSet(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSkillSet('parsedatasecuredFillSkillSet','Skill');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sector"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].ESID).html(data1[i].Description));
             }
        }
              
}

   jQuery('#Skill').on('change', function () {
    FillJobPrefrences('parsedatasecuredFillJobPrefrences','Prefrences');
       });

function FillJobPrefrences(funct,control) {
    var path =  serverpath + "jobprefrences/'" + jQuery('#Skill').val() + "'/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillJobPrefrences(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobPrefrences('parsedatasecuredFillJobPrefrences','Prefrences');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Role"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
             }
        }
              
}

