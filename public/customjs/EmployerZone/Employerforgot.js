createcaptchaforgot();
var codeforgot;
                function createcaptchaforgot() {
                  //clear the contents of captcha div first 
                  document.getElementById('captchaforgot').innerHTML = "";
                  var charsArray =
                  "0123456789";
                  var lengthOtp = 6;
                  var captcha = [];
                  for (var i = 0; i < lengthOtp; i++) {
                    //below code will not allow Repetition of Characters
                    var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                    if (captcha.indexOf(charsArray[index]) == -1)
                      captcha.push(charsArray[index]);
                    else i--;
                  }
                  var canv = document.createElement("canvas");
                  canv.id = "captchaforgot";
                  canv.width = 100;
                  canv.height = 50;
                  var ctx = canv.getContext("2d");
                  ctx.font = "25px Georgia";
                  ctx.strokeText(captcha.join(""), 0, 30);
                  //storing captcha so that can validate you can save it somewhere else according to your specific requirements
                  codeforgot = captcha.join("");
                  document.getElementById("captchaforgot").appendChild(canv); // adds the canvas to the body element
                }
                function validateCaptchaforgot() {
                  event.preventDefault();
                  
                  if (document.getElementById("cpatchatextbboxforgot").value == codeforgot) {
                     ForgotPassword() ;
                    
                    
                  }else{
                    jQuery('#cpatchatextbboxforgot').val("");
                    toastr.warning("Invalid Captcha", "", "info");
                    createcaptchaforgot();
                    jQuery('#cpatchatextbboxforgot').css('border-color', 'red'); 
                    return true;
                    
                  }
                }



$("input:radio:first").prop("checked", true).trigger("click");
$('input[type="radio"]').click(function () {
  createcaptchaforgot();
  jQuery("#cpatchatextbboxforgot").val("");
  if ($(this).attr('id') == 'email') {
    $("#useridforgot").attr("placeholder", "Enter Email Id");
    $("#useridforgot").attr("type", "text");
    $("#useridforgot").val("");
    if ($('#validuseridforgot').html() != "") {
      getvalidated('useridforgot', 'email', 'Email Id')
    }
  }
  else if ($(this).attr('id') == 'rmobile') {
    $("#useridforgot").attr("placeholder", "Enter Mobile Number");
    $("#useridforgot").attr("type", "number");
    $("#useridforgot").val("");
    if ($('#validuseridforgot').html() != "") {
      getvalidated('useridforgot', 'number', 'Mobile Number')
    }
  }
});



$("#useridforgot").keypress(function () {
  var user = $("#useridforgot").val();
  if ($("input[name=mobile]:checked").val() == "1") {
    $("#useridforgot").attr("type", "text");
  }


  if ($("input[name=mobile]:checked").val() == "2") {
    $("#useridforgot").attr("type", "number");
    if (user.length >= 10) { return false; };

  }
});
$("#useridforgot").focusout(function () {
  if ($("input[name=mobile]:checked").val() == "1") {
    getvalidated('useridforgot', 'email', 'Email Id')
  }


  if ($("input[name=mobile]:checked").val() == "2") {
    getvalidated('useridforgot', 'number', 'Mobile Number')
    //getvalidated('useridforgot', 'email', 'Email Id')

  }
});

function CheckValidation() {
  if(isValidate){
    if ($("#useridforgot").val() == '') {
      if ($("input[name=mobile]:checked").val() == "1") {
        getvalidated('useridforgot', 'email', 'Email Id')
      }
  
  
      if ($("input[name=mobile]:checked").val() == "2") {
        getvalidated('useridforgot', 'number', 'Mobile Number')
      }
    }
    if(isCaptchaValidated){
    if (jQuery('#cpatchatextbboxforgot').val() == '') {
      getvalidated('cpatchatextbboxforgot', 'text', 'Captcha')
    }
    else {
  
      validateCaptchaforgot();
  
    }
  }
  else {
    ForgotPassword();
  }
  }
  else {
    ForgotPassword();
  }
  
}

function ForgotPassword() {
  jQuery.ajax({
    type: "GET",
    contentType: "application/json; charset=utf-8",
    url: serverpath + "EmpForgotPassword/" + jQuery("#useridforgot").val() + "",
    cache: false,
    dataType: "json",
    success: function (data) {

      if (data[0][0].ReturnValue == "1") {

        toastr.success("OTP has been sent on  your Mobile Number", "", "success")
        return true;
      }
      if (data[0][0].ReturnValue == "2") {

        $('#myModalemail').modal('show');
        sessionStorage.setItem("NewEmployerId", data[0][0].EmpId);
        sessionStorage.setItem("Password", data[0][0].Pass);
        InsUpdotp('Verify');
      }
      if (data[0][0].ReturnValue == "3") {
        resetModeforget();
        jQuery("#NotRegCheck").show("");
        return true;
      }
    },
    error: function (xhr) {
      //swal(xhr.d, "", "error")
    }
  });

}


function resetModeforget() {
  jQuery("#useridforgot").val("");
  jQuery("#cpatchatextbboxforgot").val("");
  createcaptchaforgot();
  //  $('input[name=mobile]').attr('checked',false);
}


function InsUpdotp(Type) {
  if (Type == "Verify") {
    var generateOTPmaster = generateOTP();
    if(isValidate){
      sendemail(generateOTPmaster);
    }
    else{
      $("#checkemailotp").val(generateOTPmaster);
    }
  
  }
  else if (Type == "Match") {
    var generateOTPmaster = $("#checkemailotp").val();
  }
  var MasterData = {
    "p_EmpId" : sessionStorage.getItem("NewEmployerId"),
    "p_Flag" : "forgotemail",
    "p_FlagValue" :generateOTPmaster,
    "p_Type":Type

  }
  MasterData = JSON.stringify(MasterData)
  var path = serverpath + "employerverification";
  ajaxpost(path, 'parsrdataregistration', 'comment', MasterData, 'control')
}

function parsrdataregistration(data) {
  data = JSON.parse(data)
  console.log("data", data)
  if (data[0][0].ReturnValue == "3") {

    toastr.success("OTP has been sent on  your email", "", "success")
    return true;
  }
  else if (data[0][0].ReturnValue == "4") {
    $("#checkemailotp").val("");
    $('#myModalemail').modal('toggle');
    $('#modalresetpassword').modal('show');
    $("#CurrentPassword").val(sessionStorage.getItem("Password"));
    createcaptchareset();
  }
  else if (data[0][0].ReturnValue == "5") {
    $("#checkemailotp").val("");
    toastr.warning("Please Enter Correct OTP", "", "info")
    return true;
  }
}



function sendemail(OTP){
  var sub = "Email Verification OTP";
  var body = `Dear User,</div>
  <div style='font-size:18px;'><br>This is a system-generated mail that is being sent out to you with regard to your account at MP Rojgar Portal. </div>
  <div style='font-size:20px;'><br>Please use  : ${OTP} as the one-time password for secure login in your account email id This password is valid for 10 minutes.`;
sentmailglobal(jQuery("#useridforgot").val(),body,sub);
  



}


$(".toggle-password").click(function () {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});