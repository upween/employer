var codereset;

function createcaptchareset() {
    //clear the contents of captcha div first 
    document.getElementById('captchareset').innerHTML = "";
    var charsArray =
        "0123456789";
    var lengthOtp = 6;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
        //below code will not allow Repetition of Characters
        var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
        if (captcha.indexOf(charsArray[index]) == -1)
            captcha.push(charsArray[index]);
        else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captchareset";
    canv.width = 100;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    codereset = captcha.join("");
    document.getElementById("captchareset").appendChild(canv); // adds the canvas to the body element
}

function validateCaptchareset() {
    event.preventDefault();

    if (document.getElementById("cpatchaTextBoxheader").value == codereset) {
        UpdChangePassword();

    } else {
        $("#cpatchaTextBoxheader").val('');
        $("#cpatchaTextBoxheader").css('border-color', 'red');
        $("#validcpatchaTextBoxheader").html("Please enter Valid Captcha");
        createcaptchareset();



    }
}





function UpdChangePassword() {

    var MasterData = {

        "p_Password": md5(jQuery("#CurrentPassword").val()),
        "p_Emp_id": sessionStorage.getItem("NewEmployerId"),
        "p_NewPassword": md5(jQuery("#passwordfield").val()),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "EmpChangePassword";
    ajaxpost(path, 'parsrdatachangepassword', 'comment', MasterData, 'control')
}
function parsrdatachangepassword(data) {
    data = JSON.parse(data)
    if (data[0][0].ReturnValue == "1") {
        $('#modalChangepassword').modal('toggle');
        resetmode();
        sessionStorage.setItem("TypeHome","Change")
        location.reload();
        toastr.success("Password Changed Successfully", "", "success")
        return true;
    }


}
function resetmode() {
   createcaptchareset();
    jQuery("#passwordfield").val(""),
        jQuery("#Repassword").val(""),
        jQuery("#cpatchaTextBoxheader").val("")

}

function CheckChangePassword() {
if(isValidate){
 
    if (jQuery('#passwordfield').val() == "") {
        getvalidated('passwordfield', 'text', 'New Password');
        return false;
    }
    if (jQuery('#passwordfield').val() != "") {
        var password = $("#passwordfield").val();
        if (password != '') {
            var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;
    
            if (!regularExpression.test(password)) {
                $("#passwordvalidatereset").show();
                return false;
            }
            else {
                $("#passwordvalidatereset").hide();
                if (jQuery('#Repassword').val() == "") {
                    getvalidated('Repassword', 'text', 'Confirm Password');
                    return false;
                }
                if (jQuery('#Repassword').val() != jQuery('#passwordfield').val()) {
                    $('#validRepassword').html("Password Not Match");
                    jQuery('#Repassword').css('border-color', 'red');
                    return false;
                }
                else {
                    $('#validRepassword').html("");
                    jQuery('#Repassword').css('border-color', '');
            if(isCaptchaValidated){
                if (jQuery('#cpatchaTextBoxheader').val() == "") {
                    getvalidated('cpatchaTextBoxheader', 'text', 'Captcha');
                    return false;
                }
                else {
                    validateCaptchareset();
                }
            }

            else {
                UpdChangePassword();
            }
                   
                }
            }
        }

    }
}
else {
    UpdChangePassword();
}

}

$("#passwordfield").focusout(function () {
    getvalidated('passwordfield', 'text', 'New Password');

});
$("#passwordfield").keyup(function () {
    var password = $("#passwordfield").val();
    if (password != '') {
        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;

        if (!regularExpression.test(password)) {
            $("#passwordvalidatereset").show();
            return false;
        }
        else {
            $("#passwordvalidatereset").hide();

        }
    }
});

function validatepassword() {
    var password = $("#passwordfield").val();
    if (password != '') {
        var regularExpression = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{8,}$/;

        if (!regularExpression.test(password)) {
            $("#passwordvalidatereset").show();
            return false;
        }
        else {
            $("#passwordvalidatereset").hide();
            validateCaptchareset();
        }
    }
}  


$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});