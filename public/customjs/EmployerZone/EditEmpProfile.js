
function checkemail() {
    var Email;
    Email = jQuery('#txtemail').val();
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(Email) == false) {
        $("#txtemail").focus();
        toastr.warning("Please Enter Correct Email ID", "", "info")
        return true;
    }
}

$("#Pin").focusout(function(){
    checkLength('Pin','Pin','6')

  })

  function CheckValidation() {
    
    if (jQuery('#txtCompany').val() == '') {
        $(window).scrollTop(0);
        getvalidated('txtCompany','text','Establishment/Company Name')
        return false;
    }
    if (jQuery('#ddlOwnership').val() == '0') {
        $(window).scrollTop(0);
        getvalidated('ddlOwnership','select','Ownership')
        return false;
    }
    if (jQuery('#ddlsector').val() == '0') {
        $(window).scrollTop(0);
        getvalidated('ddlsector','select','Sector')
        return false;
    }
    if (jQuery('#NatureofBusiness').val() == '') {
        $(window).scrollTop(0);
        getvalidated('NatureofBusiness','text','Nature of Business')
        return false;
    }
    if (jQuery('#txtContact').val() == '') {
        
        getvalidated('txtContact','text','Name of Contact Person')
        return false;
    }
    if (jQuery('#Designation').val() == '0') {
      
        getvalidated('Designation','select','Designation')
        return false;
    }
    if (jQuery('#state').val() == '0') {
     
        getvalidated('state','select','State');
        return false;
    }
    if (jQuery('#district').val() == '0') {
        getvalidated('district','select','District')
        return false;
    }
   if (jQuery('#city').val() == '0') {
     getvalidated('city','select','City')
        return false;
    }
    if (jQuery('#permanentaddress').val() == '') {
        getvalidated('permanentaddress','text','Head Office Address')
        return false;;
    }
    if (jQuery('#presentaddress').val() == '') {
        getvalidated('presentaddress','text','Branch Office Address')
        return false;;
    }
   
    if (jQuery('#Phone').val() == '') {
        getvalidated('Phone','number','Mobile Number');
        return false;;
    }
    if (jQuery('#Email').val() == '') {
        getvalidated('Email','email','Email')
        return false;;
    }
     if (jQuery('#ceoemail').val() == '') {
        getvalidated('ceoemail','email','CEO Email')
        return false;;
    }
    if (jQuery('#EmploymentExchange').val() == '0') {
        getvalidated('EmploymentExchange','select','Employment Exchange')
        return false;;
    }
   
  
    else {
       
        InsUpdEmployerRegistration();
    }


};





function Fillsector(funct,control) {
    var path =  serverpath + "sector/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSector(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillsector('parsedatasecuredFillSector','ddlsector');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Sector"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sector_Id).html(data1[i].Description));
             }
        }
              
}


function FillOwnership(funct,control) {
    var path =  serverpath + "companytype/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillOwnership(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillOwnership('parsedatasecuredFillOwnership','ddlOwnership');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Ownership Type"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Company_type_id).html(data1[i].Company_type));
             }
        }
              
}

function Fillsectortype(funct,control) {
    var path =  serverpath + "sectortype/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillsectortype(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillsectortype('parsedatasecuredFillsectortype','ddlcompany');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Company Type"));
            
           for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_Id).html(data1[i].Des_cription));
             }
        }
              
}




function Fillempstate(funct,control) {
    var path =  serverpath + "state/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillempstate(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillempstate('parsedatasecuredFillempstate','state');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select State"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].StateId).html(data1[i].StateName));
             }
        }
              
}

jQuery('#state').on('change', function () {
    FillDistrict('parsedatasecuredFillDistrict','district',jQuery('#state').val());
  });

function FillDistrict(funct,control,state) {
    var path =  serverpath + "district/'" + state + "'/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDistrict(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrict('parsedatasecuredFillDistrict','district',$("#state").val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
        }
              
}


jQuery('#district').on('change', function () {
    FillCity('parsedatasecuredFillCity','city',jQuery('#district').val());
  });

function FillCity(funct,control,district) {
    var path =  serverpath + "city/'" + district + "'/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCity(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCity('parsedatasecuredFillCity',control,$("#district").val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#city").empty();
            var data1 = data[0];
            jQuery("#city").append(jQuery("<option ></option>").val("0").html("Select City"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#city").append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
             }
             jQuery("#city").val(control)
        }
              
}



function Fillexchangeoffice(funct,control) {
    var path =  serverpath + "exchangeoffice/0/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillexchangeoffice('parsedatasecuredFillexchangeoffice','EmploymentExchange');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
          
        }
              
}

function InsUpdEmployerRegistration() {
    sessionStorage.setItem("ceomail",jQuery("#ceoemail").val());
    
    var MasterData = {
        "p_Emp_Regno":sessionStorage.getItem("Emp_RegNo"),
        "p_Emp_RegId": sessionStorage.getItem("EmployerId"),
        "p_Ex_id": jQuery("#EmploymentExchange").val(),
        "p_Emp_RegDt":Date.now(),
        "p_CompName": jQuery("#txtCompany").val(),
        "p_NICCode":"",
        "p_CompanyProfile": jQuery("#ddlcompany").val(),
        "p_Company_type_id": jQuery("#ddlOwnership").val(),
        "p_V_Status":"0",
        "p_HO_YN": jQuery("#ddlOfficeType").val(),
        "p_TotalEmployee": jQuery("#NumberofEmployee").val(),
        "p_VerifyDt":"0000-00-00",
        "p_CancelDt":"0000-00-00",
        "p_Contact_Person": jQuery("#txtContact").val(),
        "p_Contact_Person_Desig": jQuery("#Designation").val(),
        "p_Address": jQuery("#permanentaddress").val(),
        "p_City": jQuery("#city").val(),
        "p_District_id": jQuery("#district").val(),
        "p_District_Name": "",
        "p_State_ID": jQuery("#state").val(),
        "p_Pincode": jQuery("#Pin").val(),
        "p_ContactNo": jQuery("#Landline").val(),
        "p_StdCode": jQuery("#STDCode").val(),
        "p_Email": jQuery("#Email").val(),
        "p_fax": "808080",
        "p_URL":jQuery("#Website").val(),
        "p_Business_Type": jQuery("#NatureofBusiness").val(),
        "p_E_Userid":"0",
        "p_PanNo": jQuery("#Panno").val(),
        "p_CorrespondenceAdd": jQuery("#presentaddress").val(),
        "p_ContactNo_M": jQuery("#Phone").val(),
        "p_Sector_Id": jQuery("#ddlsector").val(),
        "p_Gst_Number":$("#GSTNumber").val(),
        "p_Password":md5($("#passwordfield").val()),
        "P_GST_File":"0",
        "P_PAN_File":'0',
        "p_EntryBy":'',
        }
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "employerregistrations";
    ajaxpost(path, 'parsrdataregistration', 'comment', MasterData, 'control')
}
function parsrdataregistration(data) {
    data = JSON.parse(data)

    if (data[0][0].ReturnValue == "1") {
        toastr.warning("Already Exists ", "", "info")
        return true;
      
    }
    else if (data[0][0].ReturnValue == "2") {
        toastr.success("Submit Successfully", "", "success")
        return true;
    }
    else if (data[0][0].ReturnValue == "3") {
        toastr.success("Update Successfully", "", "success")     
           return true;
    }
}

      $('input[type="radio"]').click(function () {
        if ($(this).attr('id') == 'yesAdd') {
          $("#presentaddress").val($("#permanentaddress").val())
          $("#presentaddress").attr("disabled",true)
          $("#branchofficeaddress").hide()
        }
        else if ($(this).attr('id') == 'noAdd') {
            $("#presentaddress").val("")
            $("#presentaddress").attr("disabled",false)
            $("#branchofficeaddress").show()
        }
      });


    
    function FillDesignation() {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "adminDesignation/0/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Designation").append(jQuery("<option ></option>").val("0").html("Select Designation"));
         
                for (var i = 0; i < data[0].length; i++) {
                    jQuery("#Designation").append(jQuery("<option></option>").val(data[0][i].Designation_Id).html(data[0][i].Designation));
                }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }


   
function checkWebsite() {
    var myURL;
        myURL = jQuery('#Website').val();
       var web = new RegExp('^(https?:\\/\\/)?'+ // protocol
       '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
       '((\\d{1,3}\\.){3}\\d{1,3}))'+ // ip (v4) address
       '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ //port
       '(\\?[;&amp;a-z\\d%_.~+=-]*)?'+ // query string
       '(\\#[-a-z\\d_]*)?$','i');
         if (web.test(myURL) == false) {
            //$("#Website").focus();
        toastr.warning("Please Enter Correct Website", "", "info")
        
    }
}
    
          

function fetchEmpprofile(funct){
    Fillempstate('parsedatasecuredFillempstate','state');
    Fillexchangeoffice('parsedatasecuredFillexchangeoffice','EmploymentExchange');
    var path =  serverpath + "EmployerRegDetail/'" + sessionStorage.getItem("NewEmployerId") + "'"
    securedajaxget(path,funct,'comment','control'); 
}

function parsedatasecuredfetchEmpprofile(data){  
    data = JSON.parse(data)
    console.log(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fetchEmpprofile('parsedatasecuredfetchEmpprofile');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            
            FillDistrictEdit('parsedatasecuredFillDistrictEdit',data[0][0].District_id,$("#state").val());
            FillCity('parsedatasecuredFillCity',data[0][0].City,data[0][0].District_id?data[0][0].District_id:0);          
            $("#txtCompany").val(data[0][0].CompName);
            $("#ddlcompany").val(data[0][0].CompanyProfile);

            $("#ddlOwnership").val(data[0][0].Company_type_id);
            $("#ddlsector").val(data[0][0].Sector_Id);

            $("#NatureofBusiness").val(data[0][0].Business_Type);
            $("#Panno").val(data[0][0].PanNo);

            $("#GSTNumber").val(data[0][0].GST_Number);
            $("#ddlOfficeType").val(data[0][0].HO_YN);

            $("#NumberofEmployee").val(data[0][0].TotalEmployee);
            $("#txtContact").val(data[0][0].Contact_Person);

            $("#Designation").val(data[0][0].Contact_Person_Desig);
            $("#state").val(data[0][0].StateId);
            
            $("#district").val(data[0][0].DistrictId);

            $("#Pin").val(data[0][0].Pincode);
            $("#permanentaddress").val(data[0][0].Address);
if(data[0][0].Address!=data[0][0].CorrespondenceAdd){
    document.getElementById("noAdd").checked = true;
}
else{
    document.getElementById("yesAdd").checked = true;
}
            $("#presentaddress").val(data[0][0].CorrespondenceAdd);
            $("#STDCode").val(data[0][0].StdCode);

            $("#Landline").val(data[0][0].ContactNo);
            $("#Phone").val(data[0][0].ContactNo_M);
            
            $("#Email").val(data[0][0].Email);
            $("#EmploymentExchange").val(data[0][0].Ex_id);
            $("#Website").val(data[0][0].URL);

        }
}
   
function FillDistrictEdit(funct,control,state) {
    var path =  serverpath + "district/'" + state + "'/0/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillDistrictEdit(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillDistrictEdit('parsedatasecuredFillDistrictEdit',control,$("#state").val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#district").empty();
            var data1 = data[0];
            jQuery("#district").append(jQuery("<option ></option>").val("0").html("Select District"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#district").append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
             }
             jQuery("#district").val(control)
        }
              
}
$('input[type="radio"]').click(function () {
    if ($(this).attr('id') == 'yesAdd') {
      $("#presentaddress").val($("#permanentaddress").val())
      $("#presentaddress").attr("disabled",true)
      $("#branchofficeaddress").hide()
    }
    else if ($(this).attr('id') == 'noAdd') {
        $("#presentaddress").val("")
        $("#presentaddress").attr("disabled",false)
        $("#branchofficeaddress").show()
    }
  });