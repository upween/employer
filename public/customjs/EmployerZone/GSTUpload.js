$('#gstupload').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function(response) {
            if (response == "Error No File Selected"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Request Entity Too Large"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: PDF and Image File Only!"){
                toastr.warning(response, "", "info")
            }
            else{
                var str = response;
                
                var res = str.split("!");
                sessionStorage.setItem("GSTDoc", res[1])
                toastr.success(res[0], "", "success")
               UpdGST() 
            }
        }
    });
    return false;
 });
function UpdGST() {
    var MasterData = {
           "p_Emp_Regno" : sessionStorage.getItem("EmployerRegId"),
           "P_GST_File" :  sessionStorage.getItem("GSTDoc")
            }
   MasterData = JSON.stringify(MasterData)
   var path = serverpath + "GSTFile";
   ajaxpost(path, 'parsrdataupgst', 'comment', MasterData, 'control')
}
function parsrdataupgst(data) {
   data = JSON.parse(data)

   if (data[0][0].ReturnValue == "1") {
    window.location = '/succesreg'
   }
 
}
