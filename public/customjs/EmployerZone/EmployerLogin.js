createCaptchaemplogin()
function ValidateEmployerLogin() {
    var path = serverpath + "validateemplogin/"+$.trim(jQuery('#txtEmplogin').val())+"/"+md5(jQuery('#txtemppassword').val())
    ajaxget(path,'parsedatalogin','comment');
  }  
  function parsedatalogin(data){   
    data = JSON.parse(data)
    if (data.result.userDetails.ReturnValue == "1") {
        sessionStorage.setItem("token", data.result.token);
        sessionStorage.setItem("refreshToken", data.result.refreshToken)
        sessionStorage.setItem("EmployerId", data.result.userDetails.E_Userid);
        sessionStorage.setItem("NewEmployerId", data.result.userDetails.EmpId);
        sessionStorage.setItem("CompanyName", data.result.userDetails.U_Name);
        sessionStorage.setItem("MobileNumber", data.result.userDetails.Contact);
        sessionStorage.setItem("EmailId", data.result.userDetails.Email);
        sessionStorage.setItem("Emp_RegNo", data.result.userDetails.Emp_Regno);
        sessionStorage.setItem("Ex_id", data.result.userDetails.Ex_id);
        sessionStorage.setItem("V_Status", '1');
        sessionStorage.setItem("CompName", data.result.userDetails.CompName);
        sessionStorage.setItem("RoleId", data.result.userDetails.RoleId);

        Cookies.set('modaltype', "", { expires: 1, path: '/' });
        Cookies.set("Emp_RegNo", data.result.userDetails.Emp_Regno, { expires: 1, path: '/' });
        Cookies.set("CompanyName", data.result.userDetails.U_Name, { expires: 1, path: '/' });
        Cookies.set("MobileNumber", data.result.userDetails.Contact, { expires: 1, path: '/' });
        Cookies.set('EmailId', data.result.userDetails.Email, { expires: 1, path: '/' });
        Cookies.set('V_Status', '1', { expires: 1, path: '/' });
        Cookies.set("CompName", data.result.userDetails.CompName, { expires: 1, path: '/' });

        //RoleMenuMapping(data[0].RoleId);
        if(Cookies.get('er')=='1'){
          window.location="/ER1Form";
        }
        else{
          window.location="/Dashboard";
        }
        
    }
    else if(data.result.userDetails.ReturnValue == "2"){
        toastr.warning("Password Incorrect", "", "info")
        createCaptchaemplogin()
        return true;
      
    }
    else if(data.result.userDetails.ReturnValue == "3"){
        toastr.warning("Invalid UserId", "", "info")
        $("#cpatchaTextBoxemplogin").val("")
        createCaptchaemplogin()
        return true;
       
    }
  //   else if(data.result.userDetails.ReturnValue == "4"){
  //     toastr.warning("You are not verified", "", "info")
  //     createCaptchaemplogin()
  //     return true;
     
  // }

  else if(data.result.userDetails.ReturnValue == "4"){
    sessionStorage.setItem("token", data.result.token);
    sessionStorage.setItem("refreshToken", data.result.refreshToken)
    sessionStorage.setItem("EmployerId", data.result.userDetails.E_Userid);
    sessionStorage.setItem("NewEmployerId", data.result.userDetails.EmpId);
    sessionStorage.setItem("CompanyName", data.result.userDetails.U_Name);
    sessionStorage.setItem("MobileNumber", data.result.userDetails.Contact);
    sessionStorage.setItem("EmailId", data.result.userDetails.Email);
    sessionStorage.setItem("Emp_RegNo", data.result.userDetails.Emp_Regno);
    sessionStorage.setItem("Ex_id", data.result.userDetails.Ex_id);
    sessionStorage.setItem("V_Status", '4');
    sessionStorage.setItem("CompName", data.result.userDetails.CompName);

    Cookies.set('modaltype', "", { expires: 1, path: '/' });
    Cookies.set("Emp_RegNo", data.result.userDetails.Emp_Regno, { expires: 1, path: '/' });
    Cookies.set("CompanyName", data.result.userDetails.U_Name, { expires: 1, path: '/' });
    Cookies.set("MobileNumber", data.result.userDetails.Contact, { expires: 1, path: '/' });
    Cookies.set('EmailId', data.result.userDetails.Email, { expires: 1, path: '/' });
    Cookies.set('V_Status', '4', { expires: 1, path: '/' });
    Cookies.set("CompName", data.result.userDetails.CompName, { expires: 1, path: '/' });

    window.location="/Dashboard";
   
}

    else{
      toastr.warning(data, "", "info")
    }
  } 

function CheckValidationemp(){
  if(isValidate){
    if (jQuery('#txtEmplogin').val() == '')
    {
      getvalidated('txtEmplogin','text','Username')
      return false; 
  }
  
   if (jQuery('#txtemppassword').val() == '')
   {
    
    getvalidated('txtemppassword','text','Password')
    return false; 
 } 
  if(isCaptchaValidated){
 
if (jQuery('#cpatchaTextBoxemplogin').val() == '')
  {
    getvalidated('cpatchaTextBoxemplogin','text','Captcha')
    return false; 
}
else { 
    validateCaptchaemp() ;
 }
}
else{
  ValidateEmployerLogin();
}

  }
  else{
    ValidateEmployerLogin();
  }
  
}

var code;
function createCaptchaemplogin() {
//clear the contents of captcha div first 
document.getElementById('captchaempolyerlogin').innerHTML = "";
var charsArray =
"0123456789";
var lengthOtp = 6;
var captcha = [];
for (var i = 0; i < lengthOtp; i++) {
  //below code will not allow Repetition of Characters
  var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
  if (captcha.indexOf(charsArray[index]) == -1)
    captcha.push(charsArray[index]);
  else i--;
}
var canv = document.createElement("canvas");
canv.id = "captchaempolyerlogin";
canv.width = 100;
canv.height = 50;
var ctx = canv.getContext("2d");
ctx.font = "25px Georgia";
ctx.strokeText(captcha.join(""), 0, 30);
//storing captcha so that can validate you can save it somewhere else according to your specific requirements
code = captcha.join("");
document.getElementById("captchaempolyerlogin").appendChild(canv); // adds the canvas to the body element
}
function validateCaptchaemp() {
event.preventDefault();

if (document.getElementById("cpatchaTextBoxemplogin").value == code) {
  ValidateEmployerLogin();
}else{

 $("#cpatchaTextBoxemplogin").css('border-color', 'red');
 $("#validcpatchaTextBoxemplogin").html("Invalid Captcha.");
 createCaptchaemplogin();
 return true;
 
}
}



$(".toggle-password").click(function () {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
      input.attr("type", "text");
  } else {
      input.attr("type", "password");
  }
});