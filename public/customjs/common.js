function FillSkillsSecured(funct,control) {
    var path = serverpath + "skill/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredskill(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSkillsSecured('parsedatasecuredskill',control);
    }
    else{
        var data1 = data[0];
        jQuery("#"+control).empty();
        for (var i = 0; i < data1.length; i++) {
            jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].SkillId).html(data1[i].SkillName));
        } 
    }
}
function FillCategorySecured(funct,control){
    var path = serverpath + "category/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
  
}
function parsedatasecuredcategory(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCategorySecured('parsedatasecuredcategory',control);
    }
    else{
        var data1 = data[0];
        jQuery("#"+control).empty();
        jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Categories"));
        for (var i = 0; i < data1.length; i++) {
            jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CategoryId).html(data1[i].CategoryName));
        }
    }
}
function FillcitySecured(funct,control){
    var path = serverpath + "city/0/0/0/0"
    securedajaxget(path,funct,'comment',control);
}
function parsedatasecuredcity(data,control){
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillcitySecured('parsedatasecuredcity',control);
    }
    else{
        var data1 = data[0];
            jQuery("#"+control).empty();
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select City"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].CityId).html(data1[i].CityName));
            }
    }
}
function FillSecuredexchangeoffice(funct,control) {
    var path =  serverpath +"secured/exchangeoffice/0/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSecuredexchangeoffice(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredexchangeoffice('parsedatasecuredFillSecuredexchangeoffice',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Ex_id).html(data1[i].Ex_name));
             }
             jQuery("#"+control).val(sessionStorage.Ex_id);
        }
              
}
function FillState() {
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: serverpath + "State/0/0/0/0",
        cache: false,
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data[0].length; i++) {
                jQuery("#StateFetch").append(jQuery("<option></option>").val(data[0][i].StateId).html(data[0][i].StateName));
            }
        },
        error: function (xhr) {
            toastr.success(xhr.d, "", "error")
            return true;
        }
    });
}
               
     jQuery('#StateFetch').on('change', function () {
    FillFooterDetail(jQuery('#StateFetch').val());
});






toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": true,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
function CheckUser(){
   
}

function generateOTP() { 
          
    // Declare a digits variable  
    // which stores all digits 
    var digits = '0123456789'; 
    let OTP = ''; 
    for (let i = 0; i < 4; i++ ) { 
        OTP += digits[Math.floor(Math.random() * 10)]; 
    } 
    return OTP; 
  
 
} 
function SessionClear(){
    sessionStorage.clear();
    localStorage.clear();
    Cookies.remove('1P_JAR');
    Cookies.remove('CuterCounter_81730');
    Cookies.remove('langCookie');
    Cookies.remove('CompanyName');
    Cookies.remove('modaltype');
    Cookies.remove('EmailId');
    Cookies.remove('Emp_RegNo');
    Cookies.remove('MobileNumber');
   
}

//$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

function SessionClear(){
    sessionStorage.clear();
    localStorage.clear();
    Cookies.remove('1P_JAR');
    Cookies.remove('CuterCounter_81730');
    Cookies.remove('langCookie');
    Cookies.remove('CompanyName');
    Cookies.remove('modaltype');
    Cookies.remove('EmailId');
    Cookies.remove('Emp_RegNo');
    Cookies.remove('MobileNumber');
}
function sentmailglobal(to,body,subject) {
    var msg=`<table width='600' border='0' align='center' cellpadding='0' cellspacing='0'>
    <tr>
    <td align='left' valign='top' style='border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; border-top: solid thick #f0f0f0;'>
    <img src='http://mprojgar.gov.in/images/header_name.gif' style='width:100%'> 
    </td>
     </tr>
     <tr>
     <td align='center' valign='top' style=' border-right: solid thick #f0f0f0; border-left: solid thick #f0f0f0; background-color:#f0f0f0; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0' style='margin-top:10px;'>
     <tr>
     <td align='left' valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#525252;'>
     <div style='font-size:22px;'>
     ${body}
     </div>
     <div>
     <br> 
        For any further queries, please write to us at helpdesk.mprojgar@mp.gov.in with your current MP Rojgar portal username or call us at our toll free number 18002757751 (Monday to Saturday between 10 am – 6 pm ). 
      <br><br>
      Note: This is autogenerated email please do not reply.<br></div></td></tr></table></td></tr>
    </table>`
            var MasterData = {
                   "To":to,
                   "Msg":msg,
                   "Subject":subject
                  
            }
            MasterData = JSON.stringify(MasterData)
            var path = serverpath + "sentmail";
            ajaxpost(path, 'datasentmail', 'comment', MasterData, 'control')
         }
     function datasentmail(data) {
        data = JSON.parse(data)
     
     }
     function sentSmsGlobal(to,msg) {

        var MasterData = {
               "mobile":to,
               "message":msg,
              
        }
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "sendsms";
        ajaxpost(path, 'datasentsms', 'comment', MasterData, 'control')
     }
     function datasentsms(data) {
        data = JSON.parse(data)
     toastr.success('Email Sent Successfully')
     }
    