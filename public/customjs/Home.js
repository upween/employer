function fetchOpenVacancies() {
    if(sessionStorage.Emp_RegNo!='0'){
    var path = serverpath + "EmployerWithJobCount/"+sessionStorage.Emp_RegNo+"/'1'";
    ajaxget(path,'parsedatafetchOpenVacancies','comment','control');
    }
}
function parsedatafetchOpenVacancies(data) {
    data = JSON.parse(data)

    var data1 = data[0];
    var appenddata = "";
    var image = "";
    jQuery("#vacancytype").html("Open Vacancies");
    jQuery("#vacancylists").empty();
    for (var i = 0; i < data1.length; i++) {

        if(data1[i].EmployerLogo !=null){
            logosrc='/EmployerLogo/'+data1[i].EmployerLogo 
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }

       appenddata += "<li><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'><img src='"+logosrc+"' alt='"+data1[i].CompName+"' style='width: 86%;' /></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href='#'>"+data1[i].CompName+"</a></div><div class='location'><label class='fulltime'>Full Time</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+data1[i].Placeofwork+"</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><div class='listbtn'><button onclick=SetJobId('"+data1[i].Postid+"') style='background: blue;' class='mt-style-button normal'> Interested - "+ data1[i].AppliedJobs+"</button></div></div><div class='col-md-4 col-sm-4'><span style='color: #999;'>Posted "+data1[i].Posted+" Ago</span></div></div>      <div class='row'><div class='col-md-8 col-sm-8'><p><i class='fa fa-pencil-square' aria-hidden='true'></i> Number of vacancies : " + data1[i].TotalVacancy + "  </p></div></div>      <div class='row'><div class='col-md-6 col-sm-8'><p><i class='fa fa-briefcase' aria-hidden='true'></i> prefered " + data1[i].Desirable + "  </p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></p></div><div class='col-md-2 col-sm-2' style='margin-top: 4%;'><label class='switch'><input type='checkbox' checked id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"')><span class='slider round'></span></label></div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('PostId','"+data1[i].Postid+"');window.location='/Edit' style='width: 105px;background: blue;' class='mt-style-button normal'> Edit </button></div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('button','cancel');UpdJobStatus('"+data1[i].Postid+"') style='background: blue;' class='mt-style-button normal'> Cancel </button></div>   </div></li>";
       image ="<img src='"+logosrc+"' id='imagepro' style='border-radius:50%;margin-left:179%;margin-top:-37px;width: 70px; height: 57px;' alt=''>";
     } 
    jQuery("#vacancylists").html(appenddata);
    //jQuery("#ProfilePic").html(image);




}

function fetchClosedVacancies(type) {
    if(sessionStorage.Emp_RegNo!='0'){
   
    var path = serverpath + "EmployerWithJobCount/"+sessionStorage.Emp_RegNo+"/"+type+"";
   ajaxget(path,'parsedatafetchClosedVacancies','comment','control');
    }
}
function parsedatafetchClosedVacancies(data, control) {
   data = JSON.parse(data)

   var data1 = data[0];
   var appenddata = "";
   if(sessionStorage.getItem("Vacancies")=='Closed'){
   jQuery("#vacancytype").html("Closed Vacancies");
   }
else if (sessionStorage.getItem("Vacancies")=='Pending'){
    jQuery("#vacancytype").html("Pending Vacancies");
}

   jQuery("#vacancylists").empty();
   for (var i = 0; i < data1.length; i++) {

       if(data1[i].EmployerLogo !=null){
           logosrc='/EmployerLogo/'+data1[i].EmployerLogo 
       }
       else{
           logosrc='images/jobs/jobimg.jpg'
       }

       if (data1[i].Adt_YN == "0" ){
        var Closedbutton= "<button style='background: #f35e5e;' class='mt-style-button normal'> Closed Vacancy</button>";
          }
         else{  
          var Closedbutton= "<button style='background: #f3b442;' class='mt-style-button normal'> Pending Vacancy</button>";
        }
        

        if (data1[i].Adt_YN == "0" ){
            var switchbutton= "<label class='switch'><input type='checkbox' id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"') ><span class='slider round'></span></label>";
              }
             else{
            var switchbutton= "<label class='switch'><input type='checkbox' id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"')><span class='slider round'></span></label>";
            }

         appenddata += "<li><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'><img src='"+logosrc+"' alt='"+data1[i].CompName+"' style='width: 86%;'/></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href='#' >"+data1[i].CompName+"</a></div><div class='location'><label class='fulltime'>Full Time</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+data1[i].Placeofwork+"</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><div class='listbtn'>"+Closedbutton+"</div></div><div class='col-md-4 col-sm-4'><span style='color: #999;'>Posted "+data1[i].Posted+" Ago</span></div></div>    <div class='row'><div class='col-md-8 col-sm-8'><p><i class='fa fa-pencil-square' aria-hidden='true'></i> Number of vacancies : " + data1[i].TotalVacancy + "  </p></div></div>    <div class='row'><div class='col-md-6 col-sm-8'><p><i class='fa fa-briefcase' aria-hidden='true'></i>  prefered " + data1[i].Desirable + "  </p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></p></div><div class='col-md-2 col-sm-2' style='margin-top: 4%;'>"+switchbutton+"</div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('PostId','"+data1[i].Postid+"');window.location='/Edit' style='width: 105px;background: blue;' class='mt-style-button normal'> Edit </button></div><div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('button','cancel');UpdJobStatus('"+data1[i].Postid+"') style='background: blue;' class='mt-style-button normal'> Cancel </button></div>  </div></li>";
       
   }
   jQuery("#vacancylists").html(appenddata);

}


function fetchAllVacancies() {
    if(sessionStorage.Emp_RegNo!='0'){
    var path = serverpath + "EmployerWithJobCount/"+sessionStorage.Emp_RegNo+"/''";
   ajaxget(path,'parsedatafetchAllVacancies','comment','control');
    }
}
function parsedatafetchAllVacancies(data) {
    sessionStorage.setItem("fetchAllVacancies()",'call');

   data = JSON.parse(data)

   var data1 = data[0];
   var appenddata = "";
   jQuery("#vacancylists").empty();
   jQuery("#vacancytype").html("All Vacancies");
   for (var i = 0; i < data1.length; i++) {

    if (data1[i].Adt_YN == "0" ){
  var Closedbutton= "<button style='background: #f35e5e;' class='mt-style-button normal'> Closed Vacancy</button>";
    }
    
   else{
    var Closedbutton= "<button style='background: #288228;' class='mt-style-button normal'> Open Vacancy</button>";
  }
   if(data1[i].Adt_YN == "2"){
    var Closedbutton= "<button style='background: #f3b442;' class='mt-style-button normal'> Pending Vacancy</button>";
}
if(data1[i].Adt_YN == "4"){
    var Closedbutton= "<button style='background: #726a65;' class='mt-style-button normal'> Cancel Vacancy</button>";
}

  if (data1[i].Adt_YN == "0" ){
    var switchbutton= "<label class='switch'><input type='checkbox' id='onoffswitch"+data1[i].Postid+"'  onchange=UpdJobStatus('"+data1[i].Postid+"')><span  class='slider round'></span></label>";
    var cancel ="<button onclick=UpdJobStatus('"+data1[i].Postid+"');sessionStorage.setItem('button','cancel') style='background: blue;' class='mt-style-button normal'> Cancel </button>"
}
      else if(data1[i].Adt_YN == "2"){
        var switchbutton= "<label class='switch'><input type='checkbox' id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"')><span  class='slider round'></span></label>";
        var cancel ="<button onclick=UpdJobStatus('"+data1[i].Postid+"');sessionStorage.setItem('button','cancel') style='background: blue;' class='mt-style-button normal'> Cancel </button>"
    }
    else if(data1[i].Adt_YN == "4"){
         var switchbutton= "";
         var cancel =""
    }
     else{
        var switchbutton= "<label class='switch'><input type='checkbox' checked id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"') ><span   class='slider round'></span></label>";
        var cancel ="<button onclick=UpdJobStatus('"+data1[i].Postid+"');sessionStorage.setItem('button','cancel') style='background: blue;' class='mt-style-button normal'> Cancel </button>"

    }


       if(data1[i].EmployerLogo !=null){
           logosrc='/EmployerLogo/'+data1[i].EmployerLogo 
       }
       else{
           logosrc='images/jobs/jobimg.jpg'
       }
         appenddata += "<li><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'><img src='"+logosrc+"' alt='"+data1[i].CompName+"' style='width: 86%;'/></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href='#' >"+data1[i].CompName+"</a></div><div class='location'><label class='fulltime'>Full Time</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+data1[i].Placeofwork+"</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><a href=''><div class='listbtn'>"+Closedbutton+"</div></a></div><div class='col-md-4 col-sm-4'><span style='color: #999;'>Posted "+data1[i].Posted+" Ago</span></div></div>     <div class='row'><div class='col-md-8 col-sm-8'><p><i class='fa fa-pencil-square' aria-hidden='true'></i> Number of vacancies : " + data1[i].TotalVacancy + "  </p></div></div>      <div class='row'><div class='col-md-6 col-sm-8'><p><i class='fa fa-briefcase' aria-hidden='true'></i> prefered " + data1[i].Desirable + "  </p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></p></div><div class='col-md-2 col-sm-2' style='margin-top: 4%;'>"+switchbutton+"</div><div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('PostId','"+data1[i].Postid+"');window.location='/Edit' style='width: 105px;background: blue;' class='mt-style-button normal'> Edit </button></div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'> "+ cancel +" </div>  </div></li>";
    }
   jQuery("#vacancylists").html(appenddata);




}


function Vacanciescount(){
    if(sessionStorage.Emp_RegNo!='0'){
    var path = serverpath + "Vacancy/"+sessionStorage.Emp_RegNo+"";
   ajaxget(path,'parsedataVacanciescount','comment','control'); 
    }
}
function parsedataVacanciescount(data){
    data = JSON.parse(data)
var all='';
var Open='';
var Closed='';
var Pending='';
for (var i = 0; i < data[0].length; i++) {

    all+=""+data[0][i].p_All+"";
    Open+=""+data[0][i].p_Active+"";
    Closed+=""+data[0][i].p_InActive+"";
    Pending+=""+data[0][i].p_Pending+"";
}
   jQuery("#allvacanciescount").html(all);
   jQuery("#opencount").html(Open);
   jQuery("#closedcount").html(Closed);
   jQuery("#Pending").html(Pending);

}
function EmployerLogo(flag){
    sessionStorage.flagtype=flag;
    var MasterData = {
        "p_Emp_Regno":sessionStorage.NewEmployerId,
        "p_Employer_Logo":sessionStorage.getItem("PicFileName"),
        "p_Flag":flag
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "EmployerLogo";
    securedajaxpost(path,'parsedataEmployerLogo','comment',MasterData,'control'); 
}
function parsedataEmployerLogo(data){
    data = JSON.parse(data)
    if(data[0][0]){
        if(data[0][0].ReturnValue=='1'){
            toastr.success("Profile Upload Successful", "", "success")
            sessionStorage.setItem("PicFileName",'')
            Cookies.set('modaltype','')
            EmployerLogo('2')
            }
            else if(sessionStorage.flagtype=='2'){

                if(data[0][0].EmployerLogo=='' || data[0][0].EmployerLogo==null || data[0][0].EmployerLogo=='null' || data[0][0].EmployerLogo==undefined){
                  $('#ProfilePic').attr('src',"images/jobs/jobimg.jpg");
               
                }
                else{
                    // $('#ProfilePic').attr('src',"images/jobs/jobimg.jpg");
                    $('#ProfilePic').attr('src',"EmployerLogo/"+data[0][0].EmployerLogo);
            
                }
            }
    }


   }

function SetJobId(Id){
    
    sessionStorage.setItem("JobId",Id);
    window.location='/candidatelisting';
}


function UpdJobStatus(Id) {
    sessionStorage.setItem("JobId",Id);
   var switchstatus="";

    var status = $("#onoffswitch"+Id)[0].checked
	if (status == true){
		switchstatus = "1"
	}else{
		switchstatus="0"
    }
  
  if(sessionStorage.button=='cancel'){
        switchstatus="4"
  }
    
    sessionStorage.setItem("switchstatus",switchstatus )
    var MasterData = {
        "p_Postid":sessionStorage.getItem("JobId"),
        "p_Adt_YN":switchstatus,
        
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "UpdJobStatus";
    securedajaxpost(path, 'parsedatasecuredUpdJobStatus', 'comment', MasterData, 'control')
}

function parsedatasecuredUpdJobStatus(data) {
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        UpdJobStatus();
         } 
    // fetchOpenVacancies();
    // Vacanciescount();
    // $("#onoffswitch")[0].checked

    if(sessionStorage.getItem("Vacancies")=='Closed' &&  sessionStorage.getItem("type")=='0'){
        sessionStorage.removeItem('button')
        fetchClosedVacancies('0');
        // fetchAllVacancies();
        Vacanciescount();
       //$("#onoffswitch")[0].checked
    
    }

    else if(sessionStorage.getItem("Vacancies")=='All'){
        fetchAllVacancies();
        Vacanciescount();
       //$("#onoffswitch")[0].checked
       sessionStorage.removeItem('button')
    }

else if(sessionStorage.getItem("Vacancies")=='Open'){
    fetchOpenVacancies();
    // fetchAllVacancies();
    Vacanciescount();
   //$("#onoffswitch")[0].checked
   sessionStorage.removeItem('button')
}

else if(sessionStorage.getItem("Vacancies")=='OpenVacancies'){
    fetchOpenVacancies();
    Vacanciescount();
   //$("#onoffswitch")[0].checked
   sessionStorage.removeItem('button')
}

else if(sessionStorage.getItem("Vacancies")=='Pending'  &&  sessionStorage.getItem("type")=='2'){
    fetchClosedVacancies('2')
    Vacanciescount();
    //$("#onoffswitch")[0].checked
    sessionStorage.removeItem('button')
}

}
$('#profileimageaction').submit(function() {
    $(this).ajaxSubmit({
        error: function(xhr) {
            toastr.warning(xhr.status, "", "info")
        },
        success: function(response) {
            if (response == "Error No File Selected"){
                toastr.warning(response, "", "info")
            }
            else if (response == "File Size Limit Exceeded"){
                toastr.warning(response, "", "info")
            }
            else if (response == "Error: Image Only!"){
                toastr.warning(response, "", "info")
            }
            else{
                var str = response;
                var res = str.split("!");
                sessionStorage.setItem("PicFileName", res[1])
                toastr.success(res[0], "", "success")
                $('#ProfilePic').attr('src',"EmployerLogo/"+sessionStorage.getItem("PicFileName", res[1]));
                $('.emplogo').attr('src',"EmployerLogo/"+sessionStorage.getItem("PicFileName", res[1]));
                EmployerLogo('1',sessionStorage.getItem("PicFileName", res[1]))
            }
        }
    });
    return false;
 });










 function ForDashboardnewOpenVacancies() {
    if(sessionStorage.Emp_RegNo!='0'){
    var path = serverpath + "EmployerWithJobCount/"+sessionStorage.Emp_RegNo+"/'1'";
    ajaxget(path,'parsedataForDashboardnewOpenVacancies','comment','control');
    }
}
function parsedataForDashboardnewOpenVacancies(data) {
    data = JSON.parse(data)

    var data1 = data[0];
    var appenddata = "";
    var image = "";
    jQuery("#vacancytype").html("Open Vacancies");
    jQuery("#vacancylists").empty();
    for (var i = 0; i < data1.length; i++) {

        if(data1[i].EmployerLogo !=null){
            logosrc='/EmployerLogo/'+data1[i].EmployerLogo 
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }
        var Postingdt = data1[i].Posting_Dt.split('-');
          var Dt1 = Postingdt[2].split('');
          var Dt2 = Dt1[0]+""+Dt1[1]
          var Posting_Dt = Dt2+"-"+Postingdt[1]+"-"+Postingdt[0]



       

    //    appenddata += "<li><div class='row'><div class='col-md-8 col-sm-8'><div class='jobimg'><img src='"+logosrc+"' alt='"+data1[i].CompName+"' style='width: 86%;' /></div><div class='jobinfo'><h3><a href='#.'>" + data1[i].Designation + "</a></h3><div class='companyName'><a href='#'>"+data1[i].CompName+"</a></div><div class='location'><label class='fulltime'>Full Time</label>   - <span> <i class='fa fa-map-marker' aria-hidden='true'></i>  "+data1[i].Placeofwork+"</span></div></div><div class='clearfix'></div></div><div class='col-md-4 col-sm-4'><div class='listbtn'><button onclick=SetJobId('"+data1[i].Postid+"') style='background: blue;' class='mt-style-button normal'> Interested - "+ data1[i].AppliedJobs+"</button></div></div><div class='col-md-4 col-sm-4'><span style='color: #999;'>Posted "+data1[i].Posted+" Ago</span></div></div>      <div class='row'><div class='col-md-8 col-sm-8'><p><i class='fa fa-pencil-square' aria-hidden='true'></i> Number of vacancies : " + data1[i].TotalVacancy + "  </p></div></div>      <div class='row'><div class='col-md-6 col-sm-8'><p><i class='fa fa-briefcase' aria-hidden='true'></i> prefered " + data1[i].Desirable + "  </p><p> <i class='fa fa-money' aria-hidden='true'></i><span>  INR " + data1[i].PayandAllowances + " </span></p></div><div class='col-md-2 col-sm-2' style='margin-top: 4%;'><label class='switch'><input type='checkbox' checked id='onoffswitch"+data1[i].Postid+"' onchange=UpdJobStatus('"+data1[i].Postid+"')><span class='slider round'></span></label></div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('PostId','"+data1[i].Postid+"');window.location='/Edit' style='width: 105px;background: blue;' class='mt-style-button normal'> Edit </button></div> <div class='col-md-2 col-sm-2' style='margin-top: 3%;'><button onclick=sessionStorage.setItem('button','cancel');UpdJobStatus('"+data1[i].Postid+"') style='background: blue;' class='mt-style-button normal'> Cancel </button></div>   </div></li>";
    appenddata += "<tr><td><input type='checkbox' name='checkbox' id='"+data1[i].Postid+"' /></td><td style=' word-break: break-word;'>" + data1[i].Designation+ " <br><a href='/Edit'  onclick=sessionStorage.setItem('PostId','"+data1[i].Postid+"');>Edit</a> | <a  data-toggle='modal' data-target='#myModal' style='cursor:pointer'>Remove</a> | <a style='cursor:pointer'>SMS</a> | <a style='cursor:pointer'>Send Mail</a></td><td style=' word-break: break-word;'><a href='/candidatelist' onclick=SetJobId('"+data1[i].Postid+"')> "+ data1[i].AppliedJobs+" </a></td><td style=' word-break: break-word;'>  </td><td style=' word-break: break-word;'>" + data1[i].CompName+ "</td><td style=' word-break: break-word;'> "+ Posting_Dt +"</td></tr>";

    image ="<img src='"+logosrc+"' id='imagepro' style='border-radius:50%;margin-left:179%;margin-top:-37px;width: 70px; height: 57px;' alt=''>";
     } 
    jQuery("#vacancylists").html(appenddata);
    //jQuery("#ProfilePic").html(image);




}




function Foraccountdetails() {
    if(sessionStorage.Emp_RegNo!='0'){
    var path = serverpath + "EmployerWithJobCount/"+sessionStorage.Emp_RegNo+"/'1'";
    ajaxget(path,'parsedataForaccountdetails','comment','control');
    }
}
function parsedataForaccountdetails(data) {
    data = JSON.parse(data)

    var data1 = data[0];
    var appenddata = "";
    var image = "";
    jQuery("#vacancytype").html("Open Vacancies");
    jQuery("#vacancylists").empty();
    for (var i = 0; i < data1.length; i++) {

        if(data1[i].EmployerLogo !=null){
            logosrc='/EmployerLogo/'+data1[i].EmployerLogo 
        }
        else{
            logosrc='images/jobs/jobimg.jpg'
        }
        var Postingdt = data1[i].Posting_Dt.split('-');
          var Dt1 = Postingdt[2].split('');
          var Dt2 = Dt1[0]+""+Dt1[1]
          var Posting_Dt = Dt2+"-"+Postingdt[1]+"-"+Postingdt[0]
 appenddata += "<tr><td>"+[i+1]+"</td><td style=' word-break: break-word;'>" + data1[i].Designation+ " </td><td style=' word-break: break-word;'> "+ Posting_Dt +"</td><td style=' word-break: break-word;'><a href='/candidatelist' onclick=SetJobId('"+data1[i].Postid+"')> "+ data1[i].AppliedJobs+" </a></td></tr>";
    image ="<img src='"+logosrc+"' id='imagepro' style='border-radius:50%;margin-left:179%;margin-top:-37px;width: 70px; height: 57px;' alt=''>";
     } 
    jQuery("#vacancylists").html(appenddata);
}