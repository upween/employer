function Fillcadidatelist() {

    var MasterData = {
        "p_Designation":'',
        "p_Placeofwork": '',
     };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "SearchJob";
    ajaxpost(path, 'parsedatasecuredFillcadidatelist', 'comment', MasterData, 'joblists')
}

function parsedatasecuredFillcadidatelist(data, control) {
    data = JSON.parse(data)

    var data1 = data[0];
    var appenddata = "";
    jQuery("#joblists").empty();

    for (var i = 0; i < data1.length; i++) {
      
        {
            appenddata += "<li><div class='row'><div class='col-md-5 col-sm-5'><div class='jobimg'><img src='images/candidates/01.jpg' alt='Candidate Name' /></div><div class='jobinfo'><h3><a href='#.'>Jhon Doe</a></h3> <div class='cateinfo'>Java Developer</div> <div class='location'> New York City, New York</div></div> <div class='clearfix'></div> </div><div class='col-md-4 col-sm-4'><div class='minsalary'> <span></span></div></div><div class='col-md-3 col-sm-3'><div class='listbtn'><a href='#.'>View Profile</a></div> </div></div><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu nulla eget nisl dapibus finibus. Maecenas quis sem vel neque rhoncus dignissim. Ut et eros rhoncus, gravida tellus auctor,</p></li>";
            }
     
}
    jQuery("#joblists").html(appenddata);
}


function Candidatedetail(){
    var path = serverpath + "SearchCandidateByJobId/"+sessionStorage.JobId+"";
   ajaxget(path,'parsedataCandidatedetail','comment','control'); 
}
function parsedataCandidatedetail(data){
    data = JSON.parse(data)

    var data1 = data[0];
    var appenddata = "";
    jQuery("#joblists").empty();
    if(data1.length==0){
        appenddata = "<li><div class='row'><div class='col-md-3 col-sm-5'></div><div class='col-md-5 col-sm-5'><span style='color:grey;font-size:large'>No Candidate Interested</span></div></div></li>";
        
            }
            else{
    for (var i = 0; i < data1.length; i++) {
 
        if(data1[i].CandidateName){

        
        if(data1[i].Image !=null){
          
         var image=JobseekerPath+'Pics/'+data1[i].Image
           }
           else{
        //  $('#Image').attr('src',"images/candidates/01.jpg");
          var  image="images/jobs/jobimg.jpg"
         }
          
         if(data[0][i].Skill =='0' || data[0][i].Skill ==null){
            var skill="";
        }
        else{
            var skill= data[0][i].Skill ;   
        }
    
        if(data[0][i].Detail =='0' || data[0][i].Detail ==null){
            var detail="";
        }
        else{
            var detail= data[0][i].Detail ;   
        }
    
    
        if(data[0][i].Designation =='0' || data[0][i].Designation ==null){
            var designation="-";
        }
        else{
            var designation= data[0][i].Designation ;   
        }
            var mailBtn='';
            if(data[0][i].EmailId!=''){
                var mailBtn=` <button data-toggle='modal' data-target='#mailModal' onclick=sessionStorage.Candidateemailid='${data[0][i].EmailId};' style='background: blue;width:70%;' class='mt-style-button normal'>Send Mail</button>`;
                       
            }
        appenddata += `   <li>
        <div class='row'>
          <div class='row'>
            <div class='col-lg-2'>
              <div class='jobimg'>
                <img id='Image${i}' class='myImage' style="width: 102px;height:81px;max-width: 146%;" src='${image}' alt='Candidate Name' />
            </div>
          </div>
          <div class='col-lg-7'>
          <div class='jobinfo'>
            <h4>
            ${data1[i].CandidateName}
            </h4> 
        </div>
        <div class='cateinfo'>${designation}</div> 
        <div class='location'>${data1[i].DistrictName}</div>
          </div>
          <div class='col-lg-3'>
            <div class='listbtn'>
              <button onclick=SetCandidateId(${data1[i].CandidateId})  style='background: blue;' class='mt-style-button normal'>View Profile</button>
              </div> 
          </div>
          </div>
          <div class='row'>
            <div class='col-lg-9'>
              <p style='word-break: break-word;padding-left:6px'>${skill}</p>
              <p style='word-break: break-word;padding-left:6px'>${detail}</p>
            </div>
            <div class='col-lg-3'>
            <div class='listbtn'>
              ${mailBtn}
            </div>
              </div>
          </div>
      </div>
        </li>`;
        
          }
        }
    }
          jQuery("#joblists").html(appenddata);
        //  $('.myImage').onerror = function() {
        //     $('.myImage').src = "images/candidates/01.jpg"; 
        //   }
  
}     



function SetCandidateId(Id){
    sessionStorage.setItem("CandidateslistbySearch",'ms');
    sessionStorage.setItem("CandidateId",Id);
    window.location='/CVTemplate';
}
 function sendmailtoCandidate(){

    if($('#subject').val()==''){
        getvalidated('subject','text','Subject');
    }
  else if($('#body').val()==''){
        getvalidated('body','text','Body');
    }
    else{
        //sessionStorage.Candidateemailid
        sentmailglobal(sessionStorage.Candidateemailid,`<div style='font-size:18px'>${$('#body').val()}</div>`,$('#subject').val());
       $('#dismissModal').click();
       
    }

 }







 function NewCandidatedetail(){
  var path = serverpath + "SearchCandidateByJobId/"+sessionStorage.JobId+"";
 ajaxget(path,'parsedataNewCandidatedetail','comment','control'); 
}
function parsedataNewCandidatedetail(data){
  data = JSON.parse(data)

  var data1 = data[0];
  var appenddata = "";
  jQuery("#joblists").empty();
  if(data1.length==0){
      appenddata = "<li><div class='row'><div class='col-md-3 col-sm-5'></div><div class='col-md-5 col-sm-5'><span style='color:grey;font-size:large'>No Candidate Interested</span></div></div></li>";
      
          }
          else{
  for (var i = 0; i < data1.length; i++) {

      if(data1[i].CandidateName){

      
      if(data1[i].Image !=null){
        
       var image=JobseekerPath+'Pics/'+encodeURI(data1[i].Image)
         }
         else{
      //  $('#Image').attr('src',"images/candidates/01.jpg");
        var  image="images/jobs/jobimg.jpg"
       }
        
       if(data[0][i].Skill =='0' || data[0][i].Skill ==null){
          var skill="";
      }
      else{
          var skill= data[0][i].Skill ;   
      }
  
      if(data[0][i].Detail =='0' || data[0][i].Detail ==null){
          var detail="";
      }
      else{
          var detail= data[0][i].Detail ;   
      }
  
  
      if(data[0][i].Designation =='0' || data[0][i].Designation ==null){
          var designation="-";
      }
      else{
          var designation= data[0][i].Designation ;   
      }
          var mailBtn='';
          if(data[0][i].EmailId!=''){
              var mailBtn=` <button data-toggle='modal' data-target='#mailModal' onclick=sessionStorage.Candidateemailid='${data[0][i].EmailId};' style='background: blue;width:70%;' class='mt-style-button normal'>Send Mail</button>`;
                     
          }
    //   appenddata += `   <li>
    //   <div class='row'>
    //     <div class='row'>
    //       <div class='col-lg-2'>
    //         <div class='jobimg'>
    //           <img id='Image${i}' class='myImage' style="width: 102px;height:81px;max-width: 146%;" src='${image}' alt='Candidate Name' />
    //       </div>
    //     </div>
    //     <div class='col-lg-7'>
    //     <div class='jobinfo'>
    //       <h4>
    //       ${data1[i].CandidateName}
    //       </h4> 
    //   </div>
    //   <div class='cateinfo'>${designation}</div> 
    //   <div class='location'>${data1[i].DistrictName}</div>
    //     </div>
    //     <div class='col-lg-3'>
    //       <div class='listbtn'>
    //         <button onclick=SetCandidateId(${data1[i].CandidateId})  style='background: blue;' class='mt-style-button normal'>View Profile</button>
    //         </div> 
    //     </div>
    //     </div>
    //     <div class='row'>
    //       <div class='col-lg-9'>
    //         <p style='word-break: break-word;padding-left:6px'>${skill}</p>
    //         <p style='word-break: break-word;padding-left:6px'>${detail}</p>
    //       </div>
    //       <div class='col-lg-3'>
    //       <div class='listbtn'>
    //         ${mailBtn}
    //       </div>
    //         </div>
    //     </div>
    // </div>
    //   </li>`;
      

    appenddata += "<tr><td><input type='checkbox' name='checkbox' id='"+data1[i].CandidateId+"' /></td><td style='width: 102px;'> <img id='Image${i}' class='myImage' style='width: 85px;height:75px;max-width: 146%;' src='"+decodeURI(image)+"' alt='Candidate Name' /> </td><td>"+data1[i].CandidateName+"</td><td>"+designation+"</td><td>"+data1[i].DistrictName+"</td><td style='max-width:128px;word-break: break-word;'>"+skill+"</td><td><button   style='background: blue;' class='mt-style-button normal'> Send SMS </button></td><td><button   style='background: blue;' class='mt-style-button normal'> Send Mail </button></td><td><button onclick=SetIdforDetail('"+data1[i].CandidateId+"')  style='background: blue;' class='mt-style-button normal'>View Profile</button></td><tr>";
        }
      }
  }
        jQuery("#joblists").html(appenddata);
      //  $('.myImage').onerror = function() {
      //     $('.myImage').src = "images/candidates/01.jpg"; 
      //   }

}     

function SetIdforDetail(Id){
  sessionStorage.setItem("CandidateId",Id);
  window.location='/CandidateDetails';
}



