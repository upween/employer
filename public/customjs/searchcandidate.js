function FillSecuredSkills() {
    var path = serverpath + "skill/0/0/0/0"
    securedajaxget(path,'parsedataskillsecured','comment','control');
}
function parsedataskillsecured(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSecuredSkills();
    }
    else{
    var data1 = data[0];
    $('#Skill').selectize({
        persist: false,
        createOnBlur: true,
         valueField: 'SkillId',
        labelField: 'SkillName',
        searchField: 'SkillId',
        options: data1,
        create: true,
        maxItems:10
    });
        
    }
    }



        function FillDesignation() {
            jQuery.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: serverpath + "adminDesignation/0/0",
                cache: false,
                dataType: "json",
                success: function (data) {
                    jQuery("#Designation").append(jQuery("<option ></option>").val("0").html("Select Designation"));
             
                    for (var i = 0; i < data[0].length; i++) {
                        jQuery("#Designation").append(jQuery("<option></option>").val(data[0][i].Designation_Id).html(data[0][i].Designation));
                    }
                },
                error: function (xhr) {
                    toastr.success(xhr.d, "", "error")
                    return true;
                }
            });
        }
       
        function FillDistrict(funct,control,state) {
            var path =  serverpath + "district/0/0/0/0"
            securedajaxget(path,funct,'comment',control);
        }
        
        function parsedatasecuredFillDistrict(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDistrict('parsedatasecuredFillDistrict','District');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#"+control).empty();
                    var data1 = data[0];
                    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
                     }
                }
                      
        }


        // function Filleducation(funct,control) {
        //     var path =  serverpath + "secured/qualification/0/0/0"
        //     securedajaxget(path,funct,'comment',control);
        // }
        
        // function parsedatasecuredFilleducation(data,control){  
        //     data = JSON.parse(data)
        //     if (data.message == "New token generated"){
        //         sessionStorage.setItem("token", data.data.token);
        //         Filleducation('parsedatasecuredFilleducation','Qualification');
        //     }
        //     else if (data.status == 401){
        //         toastr.warning("Unauthorized", "", "info")
        //         return true;
        //     }
        //         else{
        //             var data1 = data[0];
        //             jQuery("#"+control).append(jQuery("<option ></option>").val('0').html("Select Education"));
        //             for (var i = 0; i < data1.length; i++) {
        //                 jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
        //             }
        //         }
                          
        // }


       
       
        
        function Fillsectortype(funct,control) {
    var path =  serverpath + "sectortype/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillsectortype(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        Fillsectortype('parsedatasecuredFillsectortype','ddlcompany');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Company Type"));
            
           for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].S_Id).html(data1[i].Des_cription));
             }
        }
              
}
function FillSkillSet(funct,control) {
    var path =  serverpath + "SkillSet/0/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillSkillSet(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillSkillSet('parsedatasecuredFillSkillSet','Skill');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            var data1 = data[0];
            $('#Skill').selectize({
                persist: false,
                createOnBlur: true,
                 valueField: 'Description',
                labelField: 'Description',
                searchField: 'Description',
                options: data1,
                create: true
            });
                
            }
            
              
}



function FillQualification() {
    //$(document).ajaxStart($.blockUI({ message: '<img src="images/loading.gif" style="width:50px;height:50px" />' })).ajaxStop($.unblockUI);

    var path = serverpath + "qualification/0/0/0"
    securedajaxget(path, 'parsedataFillQualification', 'comment', 'control');
}
function parsedataFillQualification(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillQualification();
    }
    else {
        var data1 = data[0];
        $('#Qualification').selectize({
            persist: false,
            createOnBlur: true,
            valueField: 'Qualif_id',
            labelField: 'Qualif_name',
            searchField: 'Qualif_name',
            options: data1,
            create: true
        });
    }
}



jQuery('#Qualification').on('change', function () {
    FillCourse('parsedatasecuredFillCourse','SubjectGroup',jQuery('#Qualification').val());
    FillCourse('parsedatasecuredFillCourse','Course');
       });
       
function FillCourse(funct,control,EducationId) {
    var path =  serverpath + "subjectgroup/0/'" + EducationId + "'/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredFillCourse(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillCourse('parsedatasecuredFillCourse','SubjectGroup');
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Subject Group"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Sub_Group_id).html(data1[i].Sub_Group_Name));
             }
        }
    }
   
        function redirect(){
            sessionStorage.setItem("Experience",$("#Years").val())
            sessionStorage.setItem("Designation",$("#Designation").val())
            sessionStorage.setItem("Qualification",$("#Qualification").val()==''?0:$("#Qualification").val())
            if ($("#SubjectGroup").val() == null){

            }
            else{
                sessionStorage.setItem("SubjectGroup",$("#SubjectGroup").val())
            }
            
            sessionStorage.setItem("District",$("#District").val())
            sessionStorage.setItem("Skill",$("#Skill").val())
            window.location='/CandidateslistbySearch';
        }



     