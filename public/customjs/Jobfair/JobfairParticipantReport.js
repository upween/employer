jQuery('#ddlExchangeName').on('change', function () {
    FillJobFairDetail(jQuery('#ddlExchangeName').val());
      });
function FillJobFairDetail(Ex_Id){
    
    var path =  serverpath + "empexchjfdtls/0/"+Ex_Id+"/0/"+sessionStorage.NewEmployerId+""
    securedajaxget(path,'parsedatasecuredFillJobFairDetail','comment',"control");
}
function parsedatasecuredFillJobFairDetail(data){  
    data = JSON.parse(data)
   
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDetail(jQuery('#ddlExchangeName').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#jobfairddl").empty();
            var data1 = data[0];
            for (var i = 0; i < data1.length; i++) {
              
                jQuery("#jobfairddl").append(jQuery("<option></option>").val(data1[i].Jobfair_Id).html(data1[i].Jobfair_Title+' ('+data1[i].Jobfair_FromDt+'-'+data1[i].Jobfair_ToDt+') '));
                
            }
           
    }    
    //onclick=Delete("+data1[i].Jobfair_Id+",'JFDetail')        
}
function FillParticipant(){
    var path =  serverpath + "getParticipantJobFair/"+jQuery("#jobfairddl").val()+"/"+$('#selectionstatus').val()+""
    securedajaxget(path,'parsedatasecuredFillparticipant','comment',"control");
}
function parsedatasecuredFillparticipant(data){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        FillJobFairDetail(jQuery('#ddlExchangeName').val());
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata="";
          var tablehead="";
            for (var i = 0; i < data1.length; i++) {
          if($('#selectionstatus').val()==1){
            tablehead=`<th>Sr No</th><th>Candidate Name</th>`;          
            appenddata += "<tr><td >" +[i+1]+ "</td><td>"+data1[i].CandidateName+"</td></tr>";
            $('#sendAlert').hide();
          }
          if($('#selectionstatus').val()==0){
            $('#sendAlert').show();
            tablehead=`<th>Sr No</th><th>Candidate Name</th><th>Select</th>`;          
            appenddata += "<tr><td>" +[i+1]+ "</td><td>"+data1[i].CandidateName+"</td><td><input type='checkbox' name='checkbox' id='"+data1[i].Jobfair_Id+"_"+data1[i].CandidateId+"' ></td></tr>";
          }
                
        }jQuery("#tbodyvalue").html(appenddata);  
        jQuery("#tablehead").html(tablehead); 
    }    
    //onclick=Delete("+data1[i].Jobfair_Id+",'JFDetail')        
}
function updSelectionStatus() {
   
    $('#tbodyvalue').find('input[type="checkbox"]:checked').each(function () {
       var id=this.id.split('_');
       var jobfairid=id[0];
       var candidateid=id[1];
        var MasterData ={
    
            "p_Jobfair_Id":jobfairid,
            "p_CandidateId":candidateid
            
           
            
            
            };
        MasterData = JSON.stringify(MasterData)
        var path = serverpath + "updateParticipantJobFair";
        securedajaxpost(path, 'parsedataSelectionStatus', 'comment', MasterData, 'control')
    });
    toastr.success('Submit Successful');
    jQuery("#tbodyvalue").empty();  
        jQuery("#tablehead").empty(); 
}
function parsedataSelectionStatus(data){  
    data = JSON.parse(data)
}
function CheckCandidate(){
    var seletedid=''
    var radios = document.getElementsByName("checkbox");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked)
            formValid = true;
        i++;
    }

    if (!formValid) {

        $(window).scrollTop(0);
        toastr.warning("Please Select Atleast One Candidate", "", "info")
        return false;
 }
  else{
    updSelectionStatus();
    
  }
}
