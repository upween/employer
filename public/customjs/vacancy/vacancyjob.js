
   function FillSector(funct) {
    var path = serverpath + "SkillSet/0/0"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasector(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSector('parsedatasector');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        if (data.length > 0) {
            sessionStorage.setItem('Sector', JSON.stringify(data[0]));
        }
    }
}
jQuery("#Skill").keyup(function () {
    sessionStorage.setItem('SectorId', '0');
});

jQuery("#Skill").typeahead({
    source: function (query, process) {
        var data = sessionStorage.getItem('Sector');
        description = [];
        map = {};
        var Description = "";
        
        jQuery.each(jQuery.parseJSON(data), function (i, Description) {
            map[Description.Description] = Description;
            description.push(Description.Description);
        
        });
        process(description);
    },
    minLength: 3,
    updater: function (item) {
        sessionStorage.setItem('SectorId', map[item].ESID);
        FillRole(map[item].ESID);
        return item;
    },
    
    
});
      
    // jQuery('#Skill').on('change', function () {
    //  FillRole($('#Skill').val(),"0");
    // });
    
    function FillRole(ESID) {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "jobprefrences/'" + ESID + "'/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Prefrences").empty();
        var data1 = data[0];
        jQuery("#Prefrences").append(jQuery("<option ></option>").val("0").html("Select Role"));
        for (var i = 0; i < data1.length; i++) {
        jQuery("#Prefrences").append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
         }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }
    
    function FillVacancy() {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "vacancytype/0/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Vacancy").empty();
        var data1 = data[0];
        jQuery("#Vacancy").append(jQuery("<option ></option>").val("0").html("Select Type of Vacancy"));
        for (var i = 0; i < data1.length; i++) {
        jQuery("#Vacancy").append(jQuery("<option></option>").val(data1[i].VacancyType_id).html(data1[i].VacancyType));
        }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }
    function CheckValidation() {
        if(isValidate){
           
        if (jQuery('#Skill').val() == '0') {
            $(window).scrollTop(0);
            getvalidated('Skill','select','Sector')
            return false;
        }
        
        if (jQuery('#Prefrences').val() == '0') {
            $(window).scrollTop(0);
            getvalidated('Prefrences','select','Role')
            return false;
        }
       
        if (sessionStorage.getItem("DesignationId") == '0') {
            $(window).scrollTop(0);
            getvalidated('Designation','text','Designation')
            return false;
        }
   
        if (jQuery('#numberofva').val() == '') {
            getvalidated('numberofva','text','Number of Vacancies')
            return false;
        }
       
        
    
        if (jQuery('#EssentialQualification').val() == '0') {
            getvalidated('EssentialQualification','text','Essential Qualification')
            return false;
        }
        if (jQuery('#ContactP').val() == '') {
            getvalidated('ContactP','text','Contact Person')
            return false;
            
        }
        if (jQuery('#Age').val() == '') {
            getvalidated('Age','text','As On Date')
            return false;
        }

           
         if(isCaptchaValidated){
            if (jQuery('#cpatchaTextBoxEmployerRegistration').val() == '') {
                getvalidated('cpatchaTextBoxEmployerRegistration','text','Captcha')
                return false;
            }
           else if(validateCaptcha()==true){
       
                preview();
                    
               }
        }
        else{
            preview();
        }
      
    }
    else{
        preview();
    }
        
    };
function preview(){
    $("#myModal").modal()
    $("#modalbody").html("<div class='job-header'><div class='jobdetail' style='padding: 40px;'><ul class='jbdetail'><h6>Vacancy Details</h6>"+
    "<li class='row'><div class='col-md-4 col-xs-6'>Sector:</div><div   ><span style='  color: darkgrey;  width: 102%' id='title'>"+$("#Skill option:selected").text()+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Role:</div><div   ><span style='  color: darkgrey;  width: 102%' id='details'>"+$("#Prefrences option:selected").text()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Name of Employer</div><div   ><span style='  color: darkgrey;  width: 102%'>"+ $("#nameandempadd").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Designation</div><div   ><span style='color: darkgrey;    width: 102%'>"+$("#Designation option:selected").text()+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Description</div><div   class='col-md-6 col-xs-6'  ><span style=' color: darkgrey;  text-align: left; width: 145%;'>"+$(".note-editable").html()+"</span></div></li>"+
    "<li class='row'><div class='col-md-6 col-xs-6' >Vacancy Type</div><div ><span style=' color: darkgrey;   width: 102%' >"+$("#Vacancy option:selected").text()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Number of Vacancy</div><div   ><span style='color: darkgrey;    width: 102%'>"+$("#numberofva").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Pay/Allowances</div><div   ><span style=' color: darkgrey;   width: 102%'>"+$("#minSalary").val()+' to '+$("#maxSalary").val()+' '+$('#paytype').val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Place of Work</div><div   ><span style=' color: darkgrey;   width: 102%' >"+$("#PlaceofWork option:selected").text()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Last Date To Apply</div><div   ><span style='color: darkgrey;    width: 102%'>"+$("#Lastdatetoapply").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Gender</div><div   ><span style=' color: darkgrey;   width: 102%'>"+$("#Gender option:selected").text()+"</span></div></li></ul>"+
    "<ul class='jbdetail'><h6>Eligibility Criteria</h6>"+
    "<li class='row'><div class='col-md-4 col-xs-6'>Max Age:</div><div   ><span style='  color: darkgrey;  width: 102%' id='title'>"+$("#maxAge").val()+"</span></div></li>"+
    "<li class='row'><div class='col-md-4 col-xs-6' >Essential Qualification:</div><div   ><span style='  color: darkgrey;  width: 102%' id='details'>"+$("#EssentialQualification").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Desired Qualification</div><div   ><span style='  color: darkgrey;  width: 102%'>"+ $("#DesiredQualification").val()+"</span></div></li></ul>"+
    "<ul class='jbdetail'><h6>Interview Details</h6>"+
    "<li class='row'><div class='col-md-4 col-xs-6'>Date of interview:</div><div   ><span style='  color: darkgrey;  width: 102%' id='title'>"+$("#Dateofinterview").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Time of interview:</div><div   ><span style='  color: darkgrey;  width: 102%' id='details'>"+$("#Timeofinterview").val()+"</span></div></li>"+
    "<li class='row'><div  class='col-md-4 col-xs-6'>Place of interview:</div><div   ><span style='  color: darkgrey;  width: 102%'>"+ $("#Placeofinterview").val()+"</span></div></li></ul></div></div>")
   // $("#modalbody").html("<button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px; font-size: 16px; font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick='InsUpdVacancyDetail('1')'>Post Now</button><button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px;font-size: 16px;font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick=InsUpdVacancyDetail('0')>Post Later</button>")
   $("#modalfooter").html("<button type='button' style='background: #263bd6;color: #fff;border-radius: 0;padding: 10px; font-size: 16px; font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn'>Edit</button><button type='button' style='background: #263bd6;color: #fff;border-radius: 0;padding: 10px;font-size: 16px;font-weight: 700;text-transform: uppercase;' onclick='PostJob()'  class='btn'>Save</button>")
  $("#modalhead").html("Job Preview")
}
    function PostJob(){
         $("#modalbody").html("<button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px; font-size: 16px; font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick=InsUpdVacancyDetail('1')>Post Now</button><button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px;font-size: 16px;font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick=InsUpdVacancyDetail('2')>Post Later</button>")
         $("#modalfooter").html("")
         $("#modalhead").html("Post Job")
          
    }
   
    function InsUpdVacancyDetail(Active){

        var ProbableDt_Value=""
        var ProbableDt= jQuery("#Probable").val()
           var defaultDate="01/01/1900";
        var LastDt= jQuery("#Lastdatetoapply").val()
        var InterviewDt= jQuery("#Dateofinterview").val()
        if(ProbableDt==''){
            var ProbableDt1 = defaultDate.split("/")
            ProbableDt_Value = ProbableDt1[2] + "-" + ProbableDt1[0] + "-" + ProbableDt1[1] ;
         }else{
            var ProbableDt1 = ProbableDt.split("/")
           ProbableDt_Value = ProbableDt1[2] + "-" + ProbableDt1[0] + "-" + ProbableDt1[1] ;
        }
      
        if(LastDt==''){
            var LastDt1 = defaultDate.split("/")
            var LastDt_Value = LastDt1[2] + "-" + LastDt1[0] + "-" + LastDt1[1] ;
        }
        else{
            var LastDt1 = LastDt.split("/")
            var LastDt_Value = LastDt1[2] + "-" + LastDt1[0] + "-" + LastDt1[1] ;
        }
      
        
        if(InterviewDt==''){
            var InterviewDt1 = defaultDate.split("/")
            var InterviewDt_Value = InterviewDt1[2] + "-" + InterviewDt1[1] + "-" + InterviewDt1[0] ;
        }
        else{
        var InterviewDt1 = InterviewDt.split("/")
        var InterviewDt_Value = InterviewDt1[2] + "-" + InterviewDt1[0] + "-" + InterviewDt1[1] ;
        }
        
        var AsOnDt= jQuery("#Age").val()
        var AsOnDt1 = AsOnDt.split("/")
        var AsOnDt_Value = AsOnDt1[2] + "-" + AsOnDt1[1] + "-" + AsOnDt1[0] ;
        $('.addproductrow').each(function (i) {
   
        var MasterData ={
        
        "p_Postid":'0',
        "p_Emp_Regno":sessionStorage.getItem("Emp_RegNo"), 
        "p_Ex_id":sessionStorage.getItem("Ex_id"),
        "p_Designation":  sessionStorage.getItem('DesignationId'),
        "p_JobProfile":$('.note-editable').html().replace("'"," "),
        "p_minAge":$("#minAge").val(),
        "p_maxAge":$("#maxAge").val(),
        "p_TotalVacancy":$("#numberofva"+i+"").val(),
        "p_VacancyType_id":$("#Vacancy").val(),
        "p_PayandAllowances":$("#minSalary").val() + '-' +$("#maxSalary").val()+' '+$("#paytype").val(),
        "p_Placeofwork":$("#PlaceofWork"+i+"").val(),
        "p_ProbableDt":ProbableDt_Value,
        "p_Lastdateofapply":LastDt_Value,
        "p_InterviewDt":InterviewDt_Value,
        "p_InterviewTime":$("#Timeofinterview").val(),
        "p_InterviewPlace":$("#Placeofinterview").val(),
        "p_CandidateList":"0",
        "p_Otherinfo":$("#Remark").val(),
        "p_ContactPerson":$("#ContactP").val(),
        "p_ContactNo":$("#ContactNumber").val(),
        "p_Adt_YN":Active,
        "p_Adt_Start":"2019-10-12",
        "p_Adt_End":"2019-10-12",
        "p_Qualification_essential":$("#EssentialQualification").val(),
        "p_Desirable":$("#DesiredQualification").val(),
        "p_Gender":$("#Gender").val(),
        "p_ExchEmp_Type_Id":"E",
        "p_Flag":'1',
        "p_Xge_ReceiveBy":"0",
        "p_AsOnDate":AsOnDt_Value,
        "p_Emp_id":'0',
        "p_RoleId":$("#Prefrences").val(),
        "p_SectorId":sessionStorage.getItem('SectorId')
        
        
        };
        MasterData = JSON.stringify(MasterData);
        jQuery.ajax({
            url: serverpath + "ProEmpX6",
            type: "POST",
            data: MasterData,
            contentType: "application/json; charset=utf-8",
            success: function (data, status, jqXHR) {
            if(data[0][0].Postid){
                   if(Active=='1'){
                //    SendJobAlert(jQuery('#EssentialQualification').val(),$("#Designation").val(),$("#PlaceofWork").val())
                  }
                     //ResetMode() 
                     toastr.success("Submit Successfully", "", "success")
                  
                     return true;
                   
            }  
        }
      
        });    
        });
    }
    
  function ResetMode() {
       $("#Skill").val("");
       $("#Prefrences").val("0");
    
       $("#Designation").val("");
       $(".note-editable").html("");
       $("#Vacancy").val("0");
       $("#numberofva").val("");
       $("#Payperannum").val("");
       $("#PlaceofWork").val("0");
       $("#Lastdatetoapply").val("");
       $("#Gender").val("0");
       $("#Age").val("");
       $("#EssentialQualification").val("");
       $("#DesiredQualification").val("");
       $("#Probable").val("");
       $("#Dateofinterview").val("");
       $("#Timeofinterview").val("");
       $("#Placeofinterview").val("");
       $("#Remark").val("");
       $("#ContactP").val("");
       $("#ContactNumber").val("");
       $("#minAge").val(""),
       $("#maxAge").val("")
       $("#minSalary").val("")
        $("#maxSalary").val("")
        $("#paytype").val("")
        $("#cpatchaTextBoxEmployerRegistration").val("")
        }
   

        var code;
        function createCaptchaempreg() {
        
            //clear the contents of captcha div first 
            document.getElementById('captchaEmployerRegistration').innerHTML = "";
            var charsArray =
                "0123456789";
            var lengthOtp = 6;
            var captcha = [];
            for (var i = 0; i < lengthOtp; i++) {
                //below code will not allow Repetition of Characters
                var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
                if (captcha.indexOf(charsArray[index]) == -1)
                    captcha.push(charsArray[index]);
                else i--;
            }
            var canv = document.createElement("canvas");
            canv.id = "captchaEmployerRegistration";
            canv.width = 100;
            canv.height = 50;
            var ctx = canv.getContext("2d");
            ctx.font = "25px Georgia";
            ctx.strokeText(captcha.join(""), 0, 30);
            //storing captcha so that can validate you can save it somewhere else according to your specific requirements
            code = captcha.join("");
            document.getElementById("captchaEmployerRegistration").appendChild(canv); // adds the canvas to the body element
        }
        
        function validateCaptcha() {
            event.preventDefault();
            if (document.getElementById("cpatchaTextBoxEmployerRegistration").value == code) {
                return true;
            } else {
                toastr.warning("Invalid Captcha..", "", "info")
                createCaptchaempreg();
                return false;
                
            }
        }

       
        function FillDesignation(funct) {
            var path = serverpath + "secured/adminDesignation/0/1"
            securedajaxget(path, funct, 'comment', 'control');
        }
        function parsedatasecureddesignation(data, control) {
            data = JSON.parse(data)
            if (data.message == "New token generated") {
                sessionStorage.setItem("token", data.data.token);
                FillDesignationSecured('parsedatasecureddesignation');
            }
            else if (data.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                if (data.length > 0) {
                    sessionStorage.setItem('Designation', JSON.stringify(data[0]));
                }
            }
        }
        jQuery("#Designation").keyup(function () {
            sessionStorage.setItem('DesignationId', '0');
        });
        
        jQuery("#Designation").typeahead({
            source: function (query, process) {
                var data = sessionStorage.getItem('Designation');
                designation = [];
                map = {};
                var Designation = "";
                
                jQuery.each(jQuery.parseJSON(data), function (i, Designation) {
                    map[Designation.Designation] = Designation;
                   designation.push(Designation.Designation);
                
                });
                process(designation);
            },
            minLength: 3,
            updater: function (item) {
                sessionStorage.setItem('DesignationId', map[item].Designation_Id);
                return item;
            },
            
            
        });
        function FillDistrict(funct,control,state) {
            var path =  serverpath + "district/0/0/0/0"
            securedajaxget(path,funct,'comment',control);
        }
        
        function parsedatasecuredFillDistrict(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDistrict('parsedatasecuredFillDistrict','PlaceofWork');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#"+control).empty();
                    var data1 = data[0];
                    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
                     }
                }
                      
        }


        function FillEducation(funct,control) {
            var path =  serverpath + "qualification/0/0/0"
            securedajaxget(path,funct,'comment',control);
        }
        function parsedatasecuredFillEducation(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillEducation('parsedatasecuredFillEducation','EssentialQualification');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
            var data1 = data[0];
            $('#EssentialQualification').selectize({
                persist: false,
                createOnBlur: true,
                 valueField: 'Qualif_name',
                labelField: 'Qualif_name',
                searchField: 'Qualif_name',
                options: data1,
                create: true,
                maxItems:10
            });
               
        
            }
            }
  

  
        
    var dateToday = new Date();
        var dates = $("#Probable, #Lastdatetoapply, #Dateofinterview").datepicker({
           // defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
           // numberOfMonths: 1,
            minDate: dateToday,
            onSelect: function(selectedDate) {
                var option = "minDate"
                    instance = $(this).data("datepicker"),
                 //   date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option);
            }
        });


    


  function Checklength(){

    if ($("#minAge").val() < '14'){
        $("#validminAge").html("Age cannot be less than 14")
        $("#minAge").val("")
        $("#validminAge").html("");
        $("#minAge").focus()
    }

    
   else if ($("#maxAge").val() > '65'){
         $("#validmaxAge").html("Age cannot be greater than 65")
         $("#maxAge").val("")
         $("#validminAge").val("")
         $("#maxAge").focus()
       }

   

  else  if ($("#minAge").val() > $("#maxAge").val()){
        $("#validmaxAge").html("Age cannot be less than " +$("#minAge").val())
        $("#maxAge").val("")
    }

      else{
        $("#validmaxAge").html("")
       
     }
    }

  
function SendJobAlert(EducationId,DesignationId,DistrictId) {
    var path = serverpath + "CandidateJobAlert/"+EducationId+"/"+DesignationId+"/"+DistrictId+""
    ajaxget(path,'parsedataSendJobAlert','comment');
  }  
  function parsedataSendJobAlert(data){   
    data = JSON.parse(data)
    var data1=data[0]
    for (var i = 0; i < data1.length; i++) {
        if(data1[i].jobalert_email=='1'){
            sendemail(data1[i].EmailId)
        }
        
    }
  } 
  function sendemail(emailid){
    var sub = "MP Rojgar";
var body = `<a href='#' onclick='JobseekerPath()'>
<div style='font-size:18px;color:blue'>
<br>New jobs for you in ${$("#PlaceofWork option:selected").text()} , Apply Now<br></div></a>"+
"<div style='font-size:14px;color:darkgreen'>
<br>
</div>
</br><div style='font-size:16px;color:blue'>
${$("#Designation option:selected").text()}</div><div style='font-size:13px;'>
${sessionStorage.CompanyName} - ${$("#PlaceofWork option:selected").text()}</div></br>
<div style='font-size:13px;'>${ $("#desOfduties").val()}`;
sentmailglobal(emailid,body,sub);
}
function fetchJobEdit() {

    var path = serverpath + "SearchJobbyId/'"+sessionStorage.PostId+"'"
    ajaxget(path, 'parsedataFetchJobEdit', 'comment', "control");
    }
function parsedataFetchJobEdit(data) {
    data = JSON.parse(data)
}

function addNewProductRow() {
    var x = $('.addproductrow').length;
    var productrow = `<div class="row addproductrow" id="productrow`+ x + `" >
                 
    <div class="col-md-6">

          <div class="formrow">
             <label for="example-email-input" class="col-2 col-form-label">
                <span lang="en">  
                      Place of Work(Name of Town/Village )</span>&nbsp<span class="asterisk_input label-icon"></span></label>
             <div class="formrow">
                   <select name="" id="PlaceofWork`+ x +`" class="form-control">
                    
                      </select>
               
             </div>
          </div>
        
       </div>
       <div class="col-md-4">

          <div class="formrow">
             <label for="example-email-input" class="col-2 col-form-label">
                <span lang="en">Number of Vacancies</span>&nbsp<span class="asterisk_input label-icon"></span>
             </label>
             <div class="formrow">
                <input type="text" name="name" id="numberofva`+ x +`" maxlength="5"
                   class="form-control" placeholder="Number of Vacancies" onfocusout="getvalidated('numberofva`+ x +`','text','Number of Vacancies')
                   ">
                   <span id="validnumberofva`+ x +`" style="font-size: 11px; color: #cc0033; "></span>
         
             </div>
          </div>
        
       </div>
 
 
       <div class="col-md-2 col-12 mb--20">
                                           
          <button class="btn btn--primary w-100" style=" margin-top: 18%; background-color: red;" onclick="$('#productrow`+ x+`').remove();setAmount();" >Remove </button>

    </div>
   </div>`;
    $('.mainrow').append(productrow);
    FillDistrict('parsedatasecuredFillDistrict','PlaceofWork'+x);
}
