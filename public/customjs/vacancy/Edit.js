function FillSector(funct) {
    var path = serverpath + "SkillSet/0/0"
    securedajaxget(path, funct, 'comment', 'control');
}
function parsedatasector(data, control) {
    data = JSON.parse(data)
    if (data.message == "New token generated") {
        sessionStorage.setItem("token", data.data.token);
        FillSector('parsedatasector');
    }
    else if (data.status == 401) {
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
    else {
        if (data.length > 0) {
            sessionStorage.setItem('Sector', JSON.stringify(data[0]));
        }
    }
}
      
jQuery("#Skill").keyup(function () {
    sessionStorage.setItem('SectorId', '0');
});

jQuery("#Skill").typeahead({
    source: function (query, process) {
        var data = sessionStorage.getItem('Sector');
        description = [];
        map = {};
        var Description = "";
        
        jQuery.each(jQuery.parseJSON(data), function (i, Description) {
            map[Description.Description] = Description;
            description.push(Description.Description);
        
        });
        process(description);
    },
    minLength: 3,
    updater: function (item) {
        sessionStorage.setItem('SectorId', map[item].ESID);
        return item;
    },
    
    
});
      
    
    function FillRole(ESID) {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "jobprefrences/'" + ESID + "'/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Prefrences").empty();
        var data1 = data[0];
        jQuery("#Prefrences").append(jQuery("<option ></option>").val("0").html("Select Role"));
        for (var i = 0; i < data1.length; i++) {
        jQuery("#Prefrences").append(jQuery("<option></option>").val(data1[i].FunctionalArea_id).html(data1[i].FunctionalArea));
         }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }
    
    function FillVacancy() {
        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: serverpath + "vacancytype/0/0",
            cache: false,
            dataType: "json",
            success: function (data) {
                jQuery("#Vacancy").empty();
        var data1 = data[0];
        jQuery("#Vacancy").append(jQuery("<option ></option>").val("0").html("Select Type of Vacancy"));
        for (var i = 0; i < data1.length; i++) {
        jQuery("#Vacancy").append(jQuery("<option></option>").val(data1[i].VacancyType_id).html(data1[i].VacancyType));
        }
            },
            error: function (xhr) {
                toastr.success(xhr.d, "", "error")
                return true;
            }
        });
    }
    function CheckValidation() {
        if(isValidate){
          
        if (jQuery('#Skill').val() == '0') {
            $(window).scrollTop(0);
            getvalidated('Skill','select','Sector')
            return false;
        }
        
        if (jQuery('#Prefrences').val() == '0') {
            $(window).scrollTop(0);
            getvalidated('Prefrences','select','Role')
            return false;
        }
       
        if (sessionStorage.getItem("DesignationId") == '0') {
            $(window).scrollTop(0);
            getvalidated('Designation','text','Designation')
            return false;
        }
    //    if($("#desOfduties").val()!=""){
    //     if(checkLength('desOfduties','Description of duties','100')==false){
    //         $(window).scrollTop(0);
    //         return false;
    //     }
   
    //    }
        if (jQuery('#numberofva').val() == '') {
            getvalidated('numberofva','text','Number of Vacancies')
            return false;
        }
       
        
    
        if (jQuery('#EssentialQualification').val() == '0') {
            getvalidated('EssentialQualification','text','Essential Qualification')
            return false;
        }
        if (jQuery('#ContactP').val() == '') {
            getvalidated('ContactP','text','Contact Person')
            return false;
           
        }
       else{
        InsUpdVacancyDetail('0');
       }
      
    } 
    else{
        InsUpdVacancyDetail('0');
       }
    };
    function PostJob(){
         $("#modalbody").html("<button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px; font-size: 16px; font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick=InsUpdVacancyDetail('1')>Post Now</button><button type='button' style='width: 100%;background: #263bd6;color: #fff;border-radius: 0;padding: 10px;font-size: 16px;font-weight: 700;text-transform: uppercase;'  data-dismiss='modal' class='btn' onclick=InsUpdVacancyDetail('2')>Post Later</button>")
         $("#modalfooter").html("")
         $("#modalhead").html("Post Job")
          
    }
   
    function InsUpdVacancyDetail(Active){
        var ProbableDt_Value=""
        var ProbableDt= jQuery("#Probable").val()
           var defaultDate="01/01/1900";
         //  var defaultDate="2020/01/01";
        var LastDt= jQuery("#Lastdatetoapply").val()
        var InterviewDt= jQuery("#Dateofinterview").val()
        if(ProbableDt==''){
            var ProbableDt1 = defaultDate.split("/")
            ProbableDt_Value = ProbableDt1[2] + "-" + ProbableDt1[0] + "-" + ProbableDt1[1] ;
         //   ProbableDt_Value =defaultDate
         }else{
            var ProbableDt1 = ProbableDt.split("/")
           ProbableDt_Value = ProbableDt1[2] + "-" + ProbableDt1[1] + "-" + ProbableDt1[0] ;
        }
      
        if(LastDt==''){
            var LastDt1 = defaultDate.split("/")
            var LastDt_Value = LastDt1[2] + "-" + LastDt1[1] + "-" + LastDt1[0] ;
        }
        else{
            var LastDt1 = LastDt.split("/")
            var LastDt_Value = LastDt1[2] + "-" + LastDt1[1] + "-" + LastDt1[0] ;
        }
      
        
        if(InterviewDt==''){
            var InterviewDt1 = defaultDate.split("/")
            var InterviewDt_Value = InterviewDt1[2] + "-" + InterviewDt1[0] + "-" + InterviewDt1[1] ;
        }
        else{
        var InterviewDt1 = InterviewDt.split("/")
        var InterviewDt_Value = InterviewDt1[2] + "-" + InterviewDt1[1] + "-" + InterviewDt1[0] ;
        }
        
        var AsOnDt= jQuery("#Age").val()
        var AsOnDt1 = AsOnDt.split("/")
        var AsOnDt_Value = AsOnDt1[2] + "-" + AsOnDt1[1] + "-" + AsOnDt1[0] ;

        var MasterData ={
        
        "p_Postid":sessionStorage.PostId,
        "p_Emp_Regno":sessionStorage.getItem("Emp_RegNo"), 
        "p_Ex_id":sessionStorage.getItem("Ex_id"),
        "p_Designation":  sessionStorage.getItem('DesignationId'),
        "p_JobProfile":$('.note-editable').html(),
        "p_minAge":$("#minAge").val(),
        "p_maxAge":$("#maxAge").val(),
        "p_TotalVacancy":$("#numberofva").val(),
        "p_VacancyType_id":$("#Vacancy").val(),
        "p_PayandAllowances":$("#minSalary").val() + '-' +$("#maxSalary").val()+' '+$("#paytype option:selected").val(),
        "p_Placeofwork":$("#PlaceofWork").val(),
        "p_ProbableDt":ProbableDt_Value,
        "p_Lastdateofapply":LastDt_Value,
        "p_InterviewDt":InterviewDt_Value,
        "p_InterviewTime":$("#Timeofinterview").val(),
        "p_InterviewPlace":$("#Placeofinterview").val(),
        "p_CandidateList":"0",
        "p_Otherinfo":$("#Remark").val(),
        "p_ContactPerson":$("#ContactP").val(),
        "p_ContactNo":$("#ContactNumber").val(),
        "p_Adt_YN":Active,
        "p_Adt_Start":"2019-10-12",
        "p_Adt_End":"2019-10-12",
        "p_Qualification_essential":$("#EssentialQualification").val(),
        "p_Desirable":$("#DesiredQualification").val(),
        "p_Gender":$("#Gender").val(),
        "p_ExchEmp_Type_Id":"E",
        "p_Flag":'1',
        "p_Xge_ReceiveBy":"0",
        "p_AsOnDate":AsOnDt_Value,
        "p_Emp_id":'0',
        "p_RoleId":$("#Prefrences").val(),
        "p_SectorId": sessionStorage.getItem('SectorId')
        
        
        };
        MasterData = JSON.stringify(MasterData);
        jQuery.ajax({
            url: serverpath + "ProEmpX6",
            type: "POST",
            data: MasterData,
            contentType: "application/json; charset=utf-8",
            success: function (data, status, jqXHR) {
         //console.log(data);
                ResetMode();
              window.location='/Dashboard';
       
            // if(data[0][0].Postid){
            //        if(Active=='1'){
            //     //    SendJobAlert(jQuery('#EssentialQualification').val(),$("#Designation").val(),$("#PlaceofWork").val())
            //       }
                 
                   
            
        }  
        });    
    }

  function ResetMode() {
       $("#Skill").val("0");
       $("#Prefrences").val("0");
    
       $("#Designation").val("0");
       $("#desOfduties").val("");
       $("#Vacancy").val("0");
       $("#numberofva").val("");
       $("#Payperannum").val("");
       $("#PlaceofWork").val("0");
       $("#Lastdatetoapply").val("");
       $("#Gender").val("0");
       $("#Age").val("");
       $("#EssentialQualification").val("");
       $("#DesiredQualification").val("");
       $("#Probable").val("");
       $("#Dateofinterview").val("");
       $("#Timeofinterview").val("");
       $("#Placeofinterview").val("");
       $("#Remark").val("");
       $("#ContactP").val("");
       $("#ContactNumber").val("");
       $("#minAge").val(""),
       $("#maxAge").val("")
       $("#minSalary").val("")
        $("#maxSalary").val("")
        $("#paytype").val("")
        $("#cpatchaTextBoxEmployerRegistration").val("")
        }
   

      

        function FillDesignation(funct) {
            var path = serverpath + "secured/adminDesignation/0/1"
            securedajaxget(path, funct, 'comment', 'control');
        }
        function parsedatasecureddesignation(data, control) {
            data = JSON.parse(data)
            if (data.message == "New token generated") {
                sessionStorage.setItem("token", data.data.token);
                FillDesignationSecured('parsedatasecureddesignation');
            }
            else if (data.status == 401) {
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else {
                if (data.length > 0) {
                    sessionStorage.setItem('Designation', JSON.stringify(data[0]));
                }
            }
        }
        jQuery("#Designation").keyup(function () {
            sessionStorage.setItem('DesignationId', '0');
        });
        
        jQuery("#Designation").typeahead({
            source: function (query, process) {
                var data = sessionStorage.getItem('Designation');
                designation = [];
                map = {};
                var Designation = "";
                
                jQuery.each(jQuery.parseJSON(data), function (i, Designation) {
                    map[Designation.Designation] = Designation;
                   designation.push(Designation.Designation);
                
                });
                process(designation);
            },
            minLength: 3,
            updater: function (item) {
                sessionStorage.setItem('DesignationId', map[item].Designation_Id);
                return item;
            },
            
            
        });
       
        function FillDistrict(funct,control,state) {
            var path =  serverpath + "district/0/0/0/0"
            securedajaxget(path,funct,'comment',control);
        }
        
        function parsedatasecuredFillDistrict(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillDistrict('parsedatasecuredFillDistrict','PlaceofWork');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
                else{
                    jQuery("#"+control).empty();
                    var data1 = data[0];
                    jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select District"));
                    for (var i = 0; i < data1.length; i++) {
                        jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].DistrictId).html(data1[i].DistrictName));
                     }
                }
                      
        }


        function FillEducation(funct,control) {
            var path =  serverpath + "qualification/0/0/0"
            securedajaxget(path,funct,'comment',control);
        }
        function parsedatasecuredFillEducation(data,control){  
            data = JSON.parse(data)
            if (data.message == "New token generated"){
                sessionStorage.setItem("token", data.data.token);
                FillEducation('parsedatasecuredFillEducation','EssentialQualification');
            }
            else if (data.status == 401){
                toastr.warning("Unauthorized", "", "info")
                return true;
            }
            else{
            var data1 = data[0];
            $('#EssentialQualification').selectize({
                persist: false,
                createOnBlur: true,
                 valueField: 'Qualif_name',
                labelField: 'Qualif_name',
                searchField: 'Qualif_name',
                options: data1,
                create: true,
                maxItems:10
            });
               
        
            }
            }
        // function parsedatasecuredFillEducation(data,control){  
        //     data = JSON.parse(data)
        //     if (data.message == "New token generated"){
        //         sessionStorage.setItem("token", data.data.token);
        //         FillEducation('parsedatasecuredFillEducation','EssentialQualification');
        //     }
        //     else if (data.status == 401){
        //         toastr.warning("Unauthorized", "", "info")
        //         return true;
        //     }
        //         else{
        //             var data1 = data[0];
        //             jQuery("#"+control).append(jQuery("<option ></option>").val('0').html("Select Education"));
        //             for (var i = 0; i < data1.length; i++) {
        //                 jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Qualif_id).html(data1[i].Qualif_name));
        //             }
        //         }
                          
        // }

  
        
    var dateToday = new Date();
        var dates = $("#Probable, #Lastdatetoapply, #Dateofinterview").datepicker({
           // defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
           // numberOfMonths: 1,
            minDate: dateToday,
            onSelect: function(selectedDate) {
                var option = "minDate"
                    instance = $(this).data("datepicker"),
                 //   date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option);
            }
        });


    


  function Checklength(){

    if ($("#minAge").val() < '14'){
        $("#validminAge").html("Age cannot be less than 14")
        $("#minAge").val("")
        $("#validminAge").html("");
        $("#minAge").focus()
    }

    
   else if ($("#maxAge").val() > '65'){
         $("#validmaxAge").html("Age cannot be greater than 65")
         $("#maxAge").val("")
         $("#validminAge").val("")
         $("#maxAge").focus()
       }

   

  else  if ($("#minAge").val() > $("#maxAge").val()){
        $("#validmaxAge").html("Age cannot be less than " +$("#minAge").val())
        $("#maxAge").val("")
    }

      else{
        $("#validmaxAge").html("")
       
     }
    }

  
function SendJobAlert(EducationId,DesignationId,DistrictId) {
    var path = serverpath + "CandidateJobAlert/"+EducationId+"/"+DesignationId+"/"+DistrictId+""
    ajaxget(path,'parsedataSendJobAlert','comment');
  }  
  function parsedataSendJobAlert(data){   
    data = JSON.parse(data)
    var data1=data[0]
    for (var i = 0; i < data1.length; i++) {
        if(data1[i].jobalert_email=='1'){
            sendemail(data1[i].EmailId)
        }
        
    }
  } 
  
function fetchJobEdit() {

    var path = serverpath + "SearchJobbyId/'"+sessionStorage.PostId+"'"
    ajaxget(path, 'parsedataFetchJobEdit', 'comment', "control");
    }
 function dtcon(date) {
    var dt = date.split('T');
    var dt1=dt[0].split('-');
    var dt2=dt1[2]+'/'+dt1[1]+'/'+dt1[0];
    if(dt2=="31/12/1899"){
        dt2="";
    }
    return dt2;
 }  
function parsedataFetchJobEdit(data) {
    data = JSON.parse(data);
    $('#Skill').val(data[0][0].Description);
    sessionStorage.setItem('SectorId',data[0][0].SectorId)
    FillRole(data[0][0].SectorId);
    setTimeout(function(){ $('#Prefrences').val(data[0][0].RoleId); }, 2000);
   $('#Designation').val(data[0][0].DesignationName);
   sessionStorage.setItem('DesignationId',data[0][0].Designation),
   $('.note-editable').html(data[0][0].JobProfile);

    $('#Vacancy').val(data[0][0].VacancyType_id);
    $('#numberofva').val(data[0][0].TotalVacancy);
    var payAllowances=data[0][0].PayandAllowances.split('-');
    var minSalary=payAllowances[0];
    var payAllowancemax=payAllowances[1].split(' ');
    var manSalary =payAllowancemax[0];
    var perDuration=payAllowancemax[1]+' '+payAllowancemax[2]
//var perDuration =payAllowancemax[1]+' '+payAllowancemax[2];
    $('#minSalary').val(minSalary);
    $('#maxSalary').val(manSalary);
    $('#paytype').val(perDuration);

    var lastDate =dtcon(data[0][0].Lastdateofapply);
    $('#Lastdatetoapply').val(lastDate);
    $('#PlaceofWork').val(data[0][0].Placeofwork);
    $('#Gender').val(data[0][0].Gender);
    $('#minAge').val(data[0][0].minAge);
    $('#maxAge').val(data[0][0].maxAge);
    var AsOnDate =dtcon(data[0][0].AsOnDate)
    $('#Age').val(AsOnDate);
    $('#EssentialQualification-selectized').val(data[0][0].Qualification_essential);
    $('#DesiredQualification').val(data[0][0].Desirable);
    var ProbableDate =dtcon(data[0][0].ProbableDt)
    $('#Probable').val(ProbableDate);
    var InterviewDt =dtcon(data[0][0].InterviewDt);
    $('#Dateofinterview').val(InterviewDt);
    $('#Timeofinterview').val(data[0][0].InterviewTime);
    $('#Placeofinterview').val(data[0][0].InterviewPlace);
    $('#Remark').val(data[0][0].Otherinfo);
    $('#ContactP').val(data[0][0].ContactPerson);
    $('#ContactNumber').val(data[0][0].ContactNo);



    

}
