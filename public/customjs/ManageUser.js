function fillempRole(funct,control) {
    var path =  serverpath + "getemprolemst/0/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillempole(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillempRole('parsedatasecuredfillempole',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Hiring Role"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].RoleId).html(data1[i].RoleName));
             }
        }
              
}
function fillempReporting(funct,control) {
    var path =  serverpath + "getSubUser/"+sessionStorage.NewEmployerId+"/1"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillReporting(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillempReporting('parsedatasecuredfillReporting',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            jQuery("#"+control).empty();
            var data1 = data[0];
            jQuery("#"+control).append(jQuery("<option ></option>").val("0").html("Select Reporting"));
            for (var i = 0; i < data1.length; i++) {
                jQuery("#"+control).append(jQuery("<option></option>").val(data1[i].Emp_Id).html(data1[i].Name+'('+data1[i].EmailId+')'));
             }
        }
         
        
}

$(".toggle-password").click(function () {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

function CheckSubUser(){
    $('#validRepasswordemp').html('');
    if($.trim($('#subusernametext').val())==''){
        getvalidated('subusernametext','email','User Name');
        return false;
    }
    if($.trim($('#subname').val())==''){
        getvalidated('subname','text','Name');
        return false;
    }
    if($.trim($('#subroleddl').val())==''){
        getvalidated('subroleddl','select','Hiring Role');
        return false;
    }
    
    if($.trim($('#submobile').val())==''){
        getvalidated('submobile','number','Mobile Number');
        return false;
    }
    if($.trim($('#passwordfieldemp').val())=='' && sessionStorage.SubUserId=='0'){
        getvalidated('passwordfieldemp','text','Password');
        return false;
    }
    if($.trim($('#Repasswordemp').val())=='' && sessionStorage.SubUserId=='0'){
        getvalidated('Repasswordemp','text','Confirm Password');
        return false;
    }

  
    if($.trim($('#Repasswordemp').val())!=$.trim($('#passwordfieldemp').val())  && sessionStorage.SubUserId=='0'){
        $('#validRepasswordemp').html('Password Not Matched');
        return false;
    }
    else{
        insupdSubUser();
    }
}

function insupdSubUser(){


    var MasterData = {

        "p_SubUserId":sessionStorage.SubUserId,
        "p_Emp_Id":sessionStorage.NewEmployerId,
        "p_Name":$('#subname').val(),
        "p_EmailId":$('#subusernametext').val(),
        "p_Mobile":$('#submobile').val(),
        "p_RoleId":$('#subroleddl').val(),
        "p_Reporting":$('#subreportingddl').val(),
        "p_IsActive":'1',
        "p_EntryBy":sessionStorage.NewEmployerId,
        "p_IpAddress":sessionStorage.ipAddress,
        "p_Password":md5($('#passwordfieldemp').val()),
    };
    MasterData = JSON.stringify(MasterData)
    var path = serverpath + "addSubUser";
    ajaxpost(path, 'parsrdataempsubuser', 'comment', MasterData, 'control')
}
function parsrdataempsubuser(data) {
    data = JSON.parse(data)
    if (data[0][0].ReturnValue == "1") {
        $('#modaladduser').modal('toggle');
        sessionStorage.setItem("TypeHome","Addsubuser")
        location.reload();
        toastr.success("Sub User Created Successfully", "", "success")
        sessionStorage.removeItem("SubUserId");
        return true;
    }
    else  if (data[0][0].ReturnValue == "2") {
        $('#modaladduser').modal('toggle');
       // sessionStorage.setItem("TypeHome","Addsubuser")
      //  location.reload();
      sessionStorage.removeItem("SubUserId");
        toastr.success("Sub User Edit Successfully", "", "success")
        return true;
    }  else  if (data[0][0].ReturnValue == "0") {
        $('#modaladduser').modal('toggle');
       // sessionStorage.setItem("TypeHome","Addsubuser")
      //  location.reload();
        toastr.warning("Already Exits", "", "info")
        return true;
    }


}
function fillAdduser(funct,control) {
    var path =  serverpath + "getSubUser/"+sessionStorage.NewEmployerId+"/0"
    securedajaxget(path,funct,'comment',control);
}

function parsedatasecuredfillAdduser(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        fillAdduser('parsedatasecuredfillAdduser',control);
    }
    else if (data.status == 401){
        toastr.warning("Unauthorized", "", "info")
        return true;
    }
        else{
            var data1 = data[0];
            var appenddata='';
            for (var i = 0; i < data1.length; i++) {
                
                        appenddata += "<tr><td>"+[i+1]+"</td><td>" +data1[i].Name+ "</td><td>" + data1[i].EmailId+ "</td><td>" + data1[i].Mobile+ "</td><td class='credit3'>"+data1[i].RoleId+"</td><td><button type='button'  data-toggle='modal' data-target='#modaladduser' onclick=EditUser('"+data1[i].SubUserId+"','"+encodeURI(data1[i].Name)+"','"+data1[i].RoleId+"','" + data1[i].Reporting+ "','" + data1[i].Mobile+ "') 'class='btn btn-primary' style='background-color:#716aca;border-color:#716aca;'>Edit</button></td><td><button type='button'   onclick=sessionStorage.setItem('SubUserId','"+data1[i].SubUserId+"');  class='btn btn-primary'data-toggle='modal'data-toggle='modal' data-target='#myModaldelete' style='background-color:#716aca;border-color:#716aca;'>Delete</button></td></tr>";
                        
                        $("#tbodyvalue").html(appenddata);
                
                     } 
        }
         
        
}

function EditUser(SubUserId,SubName,SubroleId,SubRepotring,SubMobile) { 
  //  modal('EditUser');
  $("#hide").hide();
  $("#edit").html('Edit Sub User');

 // $("#hide").hide();
 // $("#edit").html('Edit Sub User');

  $("#subusernametext").prop("disabled", true);
  $("#passwordfieldemp").prop("disabled", true);
  $("#password").prop("disabled", true);
  $("#Repasswordemp").prop("disabled", true);

  sessionStorage.setItem("SubUserId",SubUserId);
       // $("#subusernametext").prop("disabled", true);
       $("#subname").val(decodeURI(SubName)); 
       $("#subroleddl").val(SubroleId)  
       $("#subreportingddl").val(SubRepotring)    
       $("#submobile").val(SubMobile)



    //  $("#Institute").val(decodeURI(Institute)); 
    // $("#passYear").val(decodeURI(PassedYear))
    //$("#Speed").val(decodeURI(Speed))

    }

    
function deletesubuser(ID) {

     var path =  serverpath + "DeleteSubUser/"+ ID +" "
    securedajaxget(path,'parsedatadeletesubuser','comment','control');
}

function parsedatadeletesubuser(data,control){  
    data = JSON.parse(data)
    if (data.message == "New token generated"){
        sessionStorage.setItem("token", data.data.token);
        deletesubuser();
    }
     if (data[0][0].ReturnValue == "0") {
        $('#modaldeletesubuser').modal('toggle');       
        toastr.success("Delete Successfully", "", "success")
        fillAdduser('parsedatasecuredfillAdduser','');
        return true;
    
               
     }   
}

