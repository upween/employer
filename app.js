var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var port = process.env.PORT || 4000;
var routes = require('./routes/index');
var session = require('express-session')
var multer = require('multer');
var cors = require('cors');
var Loki = require('lokijs');
var bodyParser = require('body-parser');
const helmet = require('helmet');

var app = express();
app.use(helmet.frameguard({ action: "deny" }));
app.use(helmet.xssFilter())
app.use(helmet.noCache())
app.use(helmet.noSniff())
app.use(session({secret: 'rojgarclient'}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname,'Upload')));
// app.all('/Upload/*', function (req,res, next) {

//   if (req.session.login){
//     res.status(200).send();
//   }
//   else{
//     res.status(403).send({
//       message: 'Access Forbidden'
//     });
//   }
  
// });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
routes(app);
var storage = multer.diskStorage({
    
  destination: function (req, file, cb) {
    if (req.cookies.modaltype == "image"){
      cb(null, path.join(__dirname,'Upload/EmployerLogo/'))
    }
    if (req.cookies.modaltype == "gstdoc"){
      cb(null, path.join(__dirname,'Upload/gstdoc/'))
    }
	 if (req.cookies.modaltype == "pandoc"){
    cb(null, path.join(__dirname,'Upload/pandoc/'))
    }
    if (req.cookies.modaltype == "jobfair"){
      cb(null, path.join(__dirname,'Upload/jobfair/'))
    }
  },
  filename: function (req, file, cb) {
    //console.log(req.cookies.city)
    //var name = file.originalname
    //name=name.split(".");
    if (req.cookies.modaltype == "jobfair"){
      cb(null, req.cookies.JFTitle+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "image"){
      cb(null, req.cookies.CompanyName+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "gstdoc"){
      cb(null, 'GSTDoc'+req.cookies.CompanyName+path.extname(file.originalname))
    }
    if (req.cookies.modaltype == "pandoc"){
      cb(null, 'PANDoc'+req.cookies.CompanyName+path.extname(file.originalname))
    }
    //console.log(req);
    //callme();
  }
});
// Init Upload
const upload = multer({
  storage: storage,
  limits:{fileSize: 1000000},
  fileFilter: function(req, file, cb){
    checkFileType(req,file, cb);
  }
}).single('myFile');

function checkFileType(req,file, cb){
  // Allowed ext
  //console.log(file);
  var ext = path.extname(file.originalname);
  if (req.cookies.modaltype == "image"){
	  if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
            cb('Error: Image Only!');
        }
        
		else{
		return cb(null,true);}
  }
  if (req.cookies.modaltype == "gstdoc" || req.cookies.modaltype == "pandoc"){
	  if( ext !== '.jpg' && ext !== '.pdf' && ext !== '.jpeg' && ext !== '.png' ) {
      //console.log('Error: Document Only!');
            cb('Error: PDF and Image File Only!');
        }
      
		else{
		return cb(null,true);}
 }
 if (req.cookies.modaltype == "jobfair" ){
  if(ext !== '.docx' && ext !== '.doc' && ext !== '.jpg' && ext !== '.pdf' ) {
    //console.log('Error: Document Only!');
          cb('Error: Document Only!');
      }
    
  else{
  return cb(null,true);}
}
}


app.post('/Dashboard', (req, res) => {
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 
app.post('/GSTUpload', (req, res) => {
  
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
app.post('/PANUpload', (req, res) => {
  
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
});
app.post('/JobFairDetails', (req, res) => {
  //console.log(req);
  upload(req, res, (err) => {
    if(err){
      if (err.code == "LIMIT_FILE_SIZE"){
        return res.end("File Size Limit Exceeded");
      }
      else{
        return res.end(err);
      } 
    } else {
      if(req.file == undefined){
        res.end("Error No File Selected");
      } else {
        res.end('File Uploaded!'+req.file.filename)
      }
    }
  });
}); 
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error');
});
app.listen(port, function() {
  console.log('Server listening on port ' + port + '...');
});
module.exports = app;
